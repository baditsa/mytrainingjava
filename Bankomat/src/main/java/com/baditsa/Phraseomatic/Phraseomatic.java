package com.baditsa.Phraseomatic;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 13.03.15
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public class Phraseomatic {
    public static void main(String[] args) {

        // Создаем три набора слов для выбора
        String[] wordListOne = {"круглосуточный", "взаимный", "умный", "динамичный"};
        String[] wordListTwo = {"трудный", "ориентированный", "центральный", "сетевой"};
        String[] wordListThree = {"процесс", "талант", "подход", "портал"};

        // Вычисляем, сколько слов в каждом списке
        int oneLenght = wordListOne.length;
        int twoLenght = wordListTwo.length;
        int threeLenght = wordListThree.length;

        // Генерируем три случайных числа
        int rand1 = (int) (Math.random() * oneLenght);
        int rand2 = (int) (Math.random() * twoLenght);
        int rand3 = (int) (Math.random() * threeLenght);

        // Строим фразу
        String phrase = wordListOne[rand1] + " " + wordListTwo[rand2] + " " + wordListThree[rand3];

        // Выводим фразу на экран
        System.out.println("Все, что нам нужно, - это " + phrase);
    }
}
