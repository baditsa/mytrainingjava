package com.baditsa.WeatherStation;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 13:32
 * To change this template use File | Settings | File Templates.
 */
public interface DisplayElement {
    public void display();
}
