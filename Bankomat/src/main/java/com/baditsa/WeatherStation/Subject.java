package com.baditsa.WeatherStation;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
public interface Subject {
    public void registerObserver(Observer o);

    public void removeObserver(Observer o);

    public void notifyObservers(); // метод для оповещения наблюдателей об изменении состояния субъекта
}