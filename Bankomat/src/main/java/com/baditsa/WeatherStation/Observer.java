package com.baditsa.WeatherStation;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 13:26
 * To change this template use File | Settings | File Templates.
 */
public interface Observer {
    public void update(float temp, float humidity, float pressure); // этот интерфейс реализуется всеми наблюдателями. Все наблюдатели должны реализовать метод update()
}
