package com.baditsa.VirtualAssistant;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.07.15
 * Time: 17:10
 * To change this template use File | Settings | File Templates.
 */
public class ImageProxy implements Icon {

    ImageIcon imageIcon; // В переменной imageIcon хранится настоящий объект Icon, который должен отображаться после загрузки
    URL imageURL;
    Thread retrievalThread;
    boolean retrieving = false;

    public ImageProxy(URL url) {
        imageURL = url;
    } // Конструктору передается URL-адрес изображения - того, которое должно отображаться после загрузки

    @Override
    public int getIconWidth() {
        if (imageIcon != null) {
            return imageIcon.getIconWidth();
        } else {
            return 800;                                       // До завершения загрузки возвращаются значения
        }                                                 // длины и ширины по умолчанию. Затем управление
    }                                                     // передается imageIcon

    @Override
    public int getIconHeight() {
        if (imageIcon != null) {
            return imageIcon.getIconHeight();
        } else {
            return 600;
        }
    }

    @Override
    public void paintIcon(final Component c, Graphics g, int x, int y) { // Метод вызывается только тогда, когда требуется перерисовать изображение на экране
        if (imageIcon != null) {
            imageIcon.paintIcon(c, g, x, y); // Если объект уже существует - требование передается ему
        } else {
            g.drawString("Loading CD cover, please wait...", x + 300, y + 190); // Если нет - выводится сообщение о загрузке
            if (!retrieving) { // Если загрузка изображения еще не началась
                retrieving = true; // можно её начать
                retrievalThread = new Thread(new Runnable() { // Чтобы загрузка не парализовала пользовательский интерфейс программы, она будет выполняться в отдельном потоке
                    @Override
                    public void run() {
                        try {
                            imageIcon = new ImageIcon(imageURL, "CD Cover"); // В отдельном потоке создается экземпляр объекта Icon. Его конструктор не возвращает управление до завершения загрузки данных
                            c.repaint(); // После завершения загрузки оповещаем Swing о необходимости перерисовки
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                retrievalThread.start();
            }
        }
    }
}