/*
package com.baditsa.VirtualAssistant;

import com.baditsa.EnumerationIterator.Enumeration;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.07.15
 * Time: 18:41
 * To change this template use File | Settings | File Templates.
 *//*


public class ImageProxyTestDrive {
    ImageComponent imageComponent;
    JFrame frame = new JFrame("CD Cover Viewer");
    JMenuBar menuBar;
    JMenu menu;
    Hashtable cds = new Hashtable();

    public static void main(String[] args) throws Exception {
        ImageProxyTestDrive testDrive = new ImageProxyTestDrive();
    }

    public ImageProxyTestDrive() throws Exception{
        cds.put("Ambient: Music for Airports", "http://images.amazon.com/images/P/B000003S2K.01.LZZZZZZZ.jpg");
        cds.put("Buddha Bar", "http://images.amazon.com/images/P/B00009XBYK.01.LZZZZZZZ.jpg");
        cds.put("Ima", "http://images.amazon.com/images/P/B00005IRM.01.LZZZZZZZ.jpg");
        cds.put("Karma", "http://images.amazon.com/images/P/B00005DCB.01.LZZZZZZZ.gif");

        URL initialURL = new URL((String)cds.get("Selected Ambient Works, Vol.2"));
        menuBar = new JMenuBar();
        menu = new JMenu("Favorite CDs");
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);

        for (Enumeration e = (Enumeration) cds.keys(); e.hasMoreElements();) {
            String name = (String)e.nextElement();
            JMenuItem menuItem = new JMenu(name);
            menu.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    imageComponent.setIcon(new ImageProxy(getCDUrl(event.getActionCommand())));
                    frame.repaint();
                }
            });
        }

        // Создание панели и меню

        Icon icon = new ImageProxy(initialURL); // Создаем заместителя и связываем его с исходным URL-адресом. Каждый раз, когда в меню выбирается новый диск, программа создает нового заместителя.
        imageComponent = new ImageComponent(icon); // Затем заместитель упаковывается в компонент в компонент для добавления к объектам панели.
        frame.getContentPane().add(imageComponent); // Добавляем заместителя к объектам панели, на которой должно выводиться изображение.
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.setVisible(true);
    }

    URL getCDUrl(String name) {
        try {
            return new URL((String)cds.get(name));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}*/
