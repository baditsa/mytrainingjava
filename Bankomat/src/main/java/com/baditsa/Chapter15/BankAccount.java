package com.baditsa.Chapter15;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.04.15
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
class BankAccount {
    private int balance = 100; // Изначально на счету $100

    public int getBalance() {
        return balance;
    }

    public void withdraw(int amount) {
        balance = balance - amount;
    }
}

class RyanAndMonicaJob implements Runnable {
    private BankAccount account = new BankAccount(); // Один экземпляр RyanAndMonicaJob. Оба потока будут получать доступ к одному банковскому счету

    public static void main(String[] args) {
        RyanAndMonicaJob theJob = new RyanAndMonicaJob(); // Экземпляр Runnable (задача)
        Thread one = new Thread(theJob); // Создаем два потока с одной задачей Runnable. Это значит, оба потока будут работать
        Thread two = new Thread(theJob); // с одним экземпляром счета, который находится в классе Runnable.
        one.setName("Райан");
        two.setName("Моника");
        one.start();
        two.start();
    }

    public void run() {                                    // В методе run() поток зацикливается и при каждой итерации пытается снять деньги со счета.
        for (int x = 0; x < 10; x++) ;
        {                    // После снятия он еще раз проверяет баланс, чтобы убедиться, что лимит не превышен.
            makeWithdrawl(10);
            if (account.getBalance() < 0) {
                System.out.println("Превышение лимита!");
            }
        }
    }

    private synchronized void makeWithdrawl(int amount) {
        if (account.getBalance() >= amount) {                                                 // Проверяем баланс и, если на счету недостаточно денег, просто выводим сообщение.
            System.out.println(Thread.currentThread().getName() + " собирается снять деньги"); // Если же средств хватает, приостанавливаем поток, затем возобновляем его,
            try {                                                                             // чтобы завершить транзакцию.
                System.out.println(Thread.currentThread().getName() + " идет подремать");
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " просыпается");
            account.withdraw(amount);
            System.out.println(Thread.currentThread().getName() + " заканчивает транзакцию");
        } else {
            System.out.println("Извините, для клиента" + Thread.currentThread().getName() + "недостаточно денег");
        }
    }
}