package com.baditsa.Chapter15;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.04.15
 * Time: 12:18
 * To change this template use File | Settings | File Templates.
 */
public class DailyAdviceServer {
    String[] adviceList = {"Ешьте меньшими порциями", "Купите облегчающие джинсы", "Не годится", "Будьте честны", "Подберите другую прическу"};

    public void go() {
        try {
            ServerSocket serverSock = new ServerSocket(4242);
            while (true) { // Сервер входит в постоянный цикл, ожидая клиентских подключений
                Socket sock = serverSock.accept(); // Метод accept() блокирует приложение до тех пор, пока не поступит запрос, после чего возвращает сокет для взаимодействия с клиентом

                PrintWriter writer = new PrintWriter(sock.getOutputStream()); // Используем соединение объекта Socket с клиентом для создания экземпляра PrintWriter,
                String advice = getAdvice();
                writer.println(advice);           // после чего отпрапвляем с его помощью (println ()) строку с советом.
                writer.close();                   // Закрываем сокет, т.к. работа с клиентом закончена
                System.out.println(advice);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String getAdvice() {
        int random = (int) (Math.random() * adviceList.length);
        return adviceList[random];
    }

    public static void main(String[] args) {
        DailyAdviceServer server = new DailyAdviceServer();
        server.go();
    }
}
