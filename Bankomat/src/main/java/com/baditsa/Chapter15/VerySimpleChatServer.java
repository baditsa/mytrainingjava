package com.baditsa.Chapter15;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 14.04.15
 * Time: 13:31
 * To change this template use File | Settings | File Templates.
 */
public class VerySimpleChatServer {

    ArrayList clientOutputStreams;
    private Object go;

    public Object getGo() {
        return go;
    }

    public void setGo(Object go) {
        this.go = go;
    }

    public static class ClientHandler implements Runnable {
        BufferedReader reader;
        Socket sock;
        private ArrayList clientOutputStreams;

        public ClientHandler(Socket clientSocket) {
            try {
                sock = clientSocket;
                InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(isReader);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public void run() {
            String massage;
            try {
                while ((massage = reader.readLine()) != null) ;
                System.out.println("read " + massage);
                tellEveryone(massage);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public static void main(String[] args) {
            new VerySimpleChatServer().getGo();
        }

        public void go() {
            clientOutputStreams = new ArrayList();
            try {
                ServerSocket serverSock = new ServerSocket(5000);
                while (true) {
                    Socket clientSocket = serverSock.accept();
                    PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
                    clientOutputStreams.add(writer);

                    Thread t = new Thread(new ClientHandler(clientSocket));
                    t.start();
                    System.out.println("got a connection");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void tellEveryone(String massage) {
            Iterator it = clientOutputStreams.iterator();
            while (it.hasNext()) try {
                PrintWriter writer = (PrintWriter) it.next();
                writer.println(massage);
                writer.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
