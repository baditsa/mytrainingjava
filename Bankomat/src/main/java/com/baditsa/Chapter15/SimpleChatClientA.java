package com.baditsa.Chapter15;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.04.15
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
public class SimpleChatClientA {
    JTextArea incoming;
    JTextField outgoing;
    PrintWriter writer;
    Socket sock;
    BufferedReader reader;

    public void go() {
        JFrame frame = new JFrame("Ludicrously Simple Chat Client"); // Создаем GUI
        JPanel mainPanel = new JPanel();

        incoming = new JTextArea(15, 50);
        incoming.setLineWrap(true);
        incoming.setWrapStyleWord(true);
        incoming.setEditable(false);

        JScrollPane qScroller = new JScrollPane(incoming);
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        outgoing = new JTextField(20);
        JButton sendButton = new JButton("Send");
        sendButton.addActionListener(new SendButtonListener()); // Подключаем слушатель для событий к кнопке отправки
        mainPanel.add(qScroller);
        mainPanel.add(outgoing);
        mainPanel.add(sendButton);
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        setUpNetworking(); // Вызываем метод setUpNetworking()

        Thread readerThread = new Thread(new IncomingReader());
        readerThread.start();

        frame.setSize(400, 500);
        frame.setVisible(true);
    }

    private void setUpNetworking() {
        try {
            sock = new Socket("127.0.0.1", 5000);               // Создаем объекты Socket
            InputStreamReader streamReader = new InputStreamReader(sock.getInputStream());
            reader = new BufferedReader(streamReader);
            writer = new PrintWriter(sock.getOutputStream());   // и PrintWriter. Присваиваем PrintWriter переменной writer
            System.out.println("networking established");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private class SendButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent ev) {
            try {
                writer.println(outgoing.getText()); // Получаем текст из текстового поля и отправляем
                writer.flush();                     // его на сервер с помощью переменной writer
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            outgoing.setText("");
            outgoing.requestFocus();
        }
    }

    public static void main(String[] args) {
        new SimpleChatClientA().go();
    }

    public class IncomingReader implements Runnable {               // Это работа, которую выполняет поток. В методе run() потоквходит в цикл

        public void run() {                                         // (пока ответ сервера будет равняться null), считывает за раз одну строку
            String massage;                                         // и добавляет ее в прокручиваемую текстовую область (используя в конце символ переноса строки).
            try {
                while ((massage = reader.readLine()) != null) {
                    System.out.println("read " + massage);
                    incoming.append(massage + "\n");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
