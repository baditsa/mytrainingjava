package com.baditsa.PoorDog;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 17.03.15
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */
class PoorDog {
    private int size;
    private String name;

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }
}

class PoorDogTestDrive {
    public static void main(String[] args) {
        PoorDog one = new PoorDog();
        System.out.println("Размер собаки - " + one.getSize());
        System.out.println("Имя собаки - " + one.getName());
    }
}
