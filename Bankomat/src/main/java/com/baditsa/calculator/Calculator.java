package com.baditsa.calculator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 24.06.15
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
public class Calculator implements ActionListener {

    // объявление компонентов на уровне класса
    private JFrame window;
    private JPanel mainPanel, inputPanel, outputPanel, buttonPanel;
    private JButton jbtnAdd, jbtnSubtract, jbtnMultiply, jbtnIntDivide, jbtnRealDivide, jbtnModulo, jbtnReset, jbtnExit;
    private JLabel jlblOut, jlblNum1Caption, jlblNum2Caption;
    private JTextField jtxtNum1, jtxtNum2;

    // конструктор
    public Calculator() {
        window = new JFrame("Калькулятор");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setResizable(true);

        // контейнеры
        mainPanel = new JPanel(new BorderLayout());
        inputPanel = new JPanel();
        outputPanel = new JPanel();
        buttonPanel = new JPanel();

        inputPanel.setLayout(new GridLayout(1, 4));
        outputPanel.setLayout(new GridLayout(1, 1));
        buttonPanel.setLayout(new GridLayout(2, 4, 5, 5));

        // добавляем панели на главную панель
        mainPanel.add(inputPanel, BorderLayout.NORTH);
        mainPanel.add(outputPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        window.getContentPane().add(mainPanel); // добавляем главную панель на фрейм

        // сздаем кнопки
        jbtnAdd = new JButton("Сложить");
        jbtnSubtract = new JButton("Вычесть");
        jbtnMultiply = new JButton("Умножить");
        jbtnIntDivide = new JButton("Целочисл. деление");
        jbtnRealDivide = new JButton("Деление");
        jbtnModulo = new JButton("Остаток от деления");
        jbtnReset = new JButton("Сброс");
        jbtnExit = new JButton("Выход");

        // создаем текстовые метки
        jlblOut = new JLabel(" ", JLabel.CENTER);
        jlblNum1Caption = new JLabel("Число 1:", JLabel.RIGHT);
        jlblNum2Caption = new JLabel("Число 2:", JLabel.RIGHT);

        // создаем текстовые поля
        jtxtNum1 = new JTextField();
        jtxtNum2 = new JTextField();

        // слушатели для кнопок
        jbtnAdd.addActionListener(this);
        jbtnSubtract.addActionListener(this);
        jbtnMultiply.addActionListener(this);
        jbtnIntDivide.addActionListener(this);
        jbtnRealDivide.addActionListener(this);
        jbtnModulo.addActionListener(this);
        jbtnReset.addActionListener(this);
        jbtnExit.addActionListener(this);

        // добавляем кнопки на панель
        buttonPanel.add(jbtnAdd);
        buttonPanel.add(jbtnSubtract);
        buttonPanel.add(jbtnMultiply);
        buttonPanel.add(jbtnIntDivide);
        buttonPanel.add(jbtnRealDivide);
        buttonPanel.add(jbtnModulo);
        buttonPanel.add(jbtnReset);
        buttonPanel.add(jbtnExit);

        // добавляем текстовые метки и поля на панели
        inputPanel.add(jlblNum1Caption);
        inputPanel.add(jtxtNum1);
        inputPanel.add(jlblNum2Caption);
        inputPanel.add(jtxtNum2);

        outputPanel.add(jlblOut);

        window.pack(); // устанавливаем необходимый размер фрейма
        window.setLocationRelativeTo(null); // фрейм появится в центре экрана
        window.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // определить нажатую кнопку и выполнить соответствующую операцию
        if (e.getSource() == jbtnAdd) {
            add();
        } else if (e.getSource() == jbtnSubtract) {
            subtract();
        } else if (e.getSource() == jbtnMultiply) {
            multiply();
        } else if (e.getSource() == jbtnIntDivide) {
            intDivide();
        } else if (e.getSource() == jbtnRealDivide) {
            realDivide();
        } else if (e.getSource() == jbtnModulo) {
            modulo();
        } else if (e.getSource() == jbtnReset) {
            reset();
        } else if (e.getSource() == jbtnExit) {
            exit();
        }
    }

    //----------------------------------------------------------------------------------------------------------------------
    // сложить
    private void add() {
        int num1, num2, result;

        // проверить введенные данные
        if ((isValidInput(jtxtNum1, "Число 1")) && (isValidInput(jtxtNum2, "Число 2"))) {

            // получить введенные числа
            num1 = Integer.parseInt(jtxtNum1.getText());
            num2 = Integer.parseInt(jtxtNum2.getText());

            // сделать расчет
            result = num1 + num2;

            // записать результат
            displayData(num1, num2, "+", result);
        }
    }

    // вычесть
    private void subtract() {
        int num1, num2, result;

        // проверить введенные данные
        if ((isValidInput(jtxtNum1, "Число 1")) && (isValidInput(jtxtNum2, "Число 2"))) {

            // получить введенные числа
            num1 = Integer.parseInt(jtxtNum1.getText());
            num2 = Integer.parseInt(jtxtNum2.getText());

            // сделать расчет
            result = num1 - num2;

            // записать результат
            displayData(num1, num2, "-", result);
        }
    }

    // умножить
    private void multiply() {
        int num1, num2, result;

        // проверить введенные данные
        if ((isValidInput(jtxtNum1, "Число 1")) && (isValidInput(jtxtNum2, "Число 2"))) {

            // получить введенные числа
            num1 = Integer.parseInt(jtxtNum1.getText());
            num2 = Integer.parseInt(jtxtNum2.getText());

            // сделать расчет
            result = num1 * num2;

            // записать результат
            displayData(num1, num2, "*", result);
        }
    }

    // целочисленное деление
    private void intDivide() {
        int num1, num2, result;

        // проверить введенные данные
        if ((isValidInput(jtxtNum1, "Число 1")) && (isValidInput(jtxtNum2, "Число 2"))) {

            // получить введенные числа
            num1 = Integer.parseInt(jtxtNum1.getText());
            num2 = Integer.parseInt(jtxtNum2.getText());

            // сделать расчет
            result = num1 / num2;

            // записать результат
            displayData(num1, num2, "/", result);
        }
    }

    // деление
    private void realDivide() {
        int num1, num2;
        double result;

        // проверить введенные данные
        if ((isValidInput(jtxtNum1, "Число 1")) && (isValidInput(jtxtNum2, "Число 2"))) {

            // получить введенные числа
            num1 = Integer.parseInt(jtxtNum1.getText());
            num2 = Integer.parseInt(jtxtNum2.getText());

            // сделать расчет
            result = (double) num1 / (double) num2;

            // записать результат
            displayData(num1, num2, "/", result);
        }
    }

    // остаток от деления
    private void modulo() {
        int num1, num2, result;

        // проверить введенные данные
        if ((isValidInput(jtxtNum1, "Число 1")) && (isValidInput(jtxtNum2, "Число 2"))) {

            // получить введенные числа
            num1 = Integer.parseInt(jtxtNum1.getText());
            num2 = Integer.parseInt(jtxtNum2.getText());

            // сделать расчет
            result = num1 % num2;

            // записать результат
            displayData(num1, num2, "%", result);
        }
    }

    // сброс
    private void reset() {
        jlblOut.setText(" ");
        jtxtNum1.setText("");
        jtxtNum2.setText("");
        jtxtNum1.requestFocus();
    }

    // выход
    private void exit() {
        // показать пользователю сообщениедля подтверждения
        int reponse = JOptionPane.showConfirmDialog(null, "Вы действительно хотите закрыть приложение?", "Подтверждение", JOptionPane.YES_NO_OPTION);

        // если пользователь нажал ДА
        if (reponse == JOptionPane.YES_OPTION) {

            // выйти из программы
            window.dispose();
            System.exit(0);
        }
    }

    //----------------------------------------------------------------------------------------------------------------------
    // отображение результатов расчета, где результат будет целое число
    private void displayData(int num1, int num2, String op, int result) {
        // форматированный вывод
        String s = String.format("%d %s %d = %d", num1, op, num2, result);
        // записать результат в текстовую метку
        jlblOut.setText(s);
    }

    // перегруженный метод - отображение результатов расчета
    private void displayData(int num1, int num2, String op, double result) {
        // форматированный вывод
        String s = String.format("%d %s %d = %.2f", num1, op, num2, result);
        // записать результат в текстовую метку
        jlblOut.setText(s);
    }

    //----------------------------------------------------------------------------------------------------------------------
    private boolean isValidInput(JTextField jtxt, String description) {
        // если был введен какой-либо текст
        if (!jtxt.getText().trim().isEmpty()) {
            // проверка на ввод только целого числа
            try {
                // попытка преобразовать текст в целое число
                int num = Integer.parseInt(jtxt.getText());
                // если все нормально возвращаем true
                return true;
            } catch (NumberFormatException e) {

                // предупреждение - неверный формат числа
                JOptionPane.showMessageDialog(window, "Вы должны ввести целое число!", "Ошибка", JOptionPane.WARNING_MESSAGE);

                jtxt.requestFocus();
                jtxt.selectAll();
                return false;
            }
        } else { // если пользователь не ввел никаких данных
            // предупреждение, что нужно ввести данные
            JOptionPane.showMessageDialog(window, "Введите число " + description, "Ошибка", JOptionPane.WARNING_MESSAGE);

            jtxt.requestFocus();
            jtxt.selectAll();
            return false;
        }
    }

    //----------------------------------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setGUI();
            }
        });
    }

    private static void setGUI() {
        Calculator gui = new Calculator();
    }
}