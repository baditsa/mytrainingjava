package com.baditsa.Chapter13;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 07.04.15
 * Time: 15:16
 * To change this template use File | Settings | File Templates.
 */
public class BeatBox {
    JPanel mainPanel;
    ArrayList<JCheckBox> checkBoxList; // Храним флажки в массиве ArrayList
    Sequencer sequencer;
    Sequence sequence;
    Track track;
    JFrame theFrame;

    String[] instrumentNames = {"Bass Drum", "Closed Hi-Hat", "Open Hi-Hat", "Acoustic Snare", "Crash Cymbal", "Hand Clap",
            "High Tom", "Hi Bongo", "Maracas", "Whistle", "Low Conga", "Cowbell", "Vibraslap", "Low-mid Tom", "High Agogo", "Open Hi Conga"};

    int[] instruments = {35, 42, 46, 38, 49, 39, 50, 60, 70, 72, 64, 56, 58, 47, 67, 63};

    public static void main(String[] args) {
        new BeatBox().buildGUI();
    }

    public void buildGUI() {
        theFrame = new JFrame("Cyber BeatBox");
        theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BorderLayout layout = new BorderLayout();
        JPanel background = new JPanel(layout);
        background.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        checkBoxList = new ArrayList<JCheckBox>();
        Box buttonBox = new Box(BoxLayout.Y_AXIS);

        JButton start = new JButton("Start");
        start.addActionListener(new MyStartListener());
        buttonBox.add(start);

        JButton stop = new JButton("Stop");
        stop.addActionListener(new MyStopListener());
        buttonBox.add(stop);

        JButton upTempo = new JButton("Tempo Up");
        upTempo.addActionListener(new MyUpTempoListener());
        buttonBox.add(upTempo);

        JButton downTempo = new JButton("Tempo Down");
        upTempo.addActionListener(new MyDownTempoListener());
        buttonBox.add(downTempo);

        Box nameBox = new Box(BoxLayout.Y_AXIS);
        for (int i = 0; i < 16; i++) {
            nameBox.add(new Label(instrumentNames[i]));
        }

        background.add(BorderLayout.EAST, buttonBox);
        background.add(BorderLayout.WEST, nameBox);

        theFrame.getContentPane().add(background);

        GridLayout grid = new GridLayout(16, 16);
        grid.setVgap(1);
        grid.setHgap(2);
        mainPanel = new JPanel(grid);
        background.add(BorderLayout.CENTER, mainPanel);

        for (int i = 0; i < 256; i++) {
            JCheckBox c = new JCheckBox();
            c.setSelected(false);
            checkBoxList.add(c);
            mainPanel.add(c);
        }

        setUpMidi();

        theFrame.setBounds(50, 50, 300, 300);
        theFrame.pack();
        theFrame.setVisible(true);
    }

    public void setUpMidi() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequence = new Sequence(Sequence.PPQ, 4);
            track = sequence.createTrack();
            sequencer.setTempoInBPM(120);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void buildTrackAndStart() {
        int[] trackList = null; // Создаем массив из 16 элементов, чтобы хранить значения для каждого инструмента, на все 16 тактов

        sequence.deleteTrack(track); // Избавляемся от старой дорожки
        track = sequence.createTrack(); // и создаем новую

        for (int i = 0; i < 16; i++) { // Делаем это для каждого из 16 рядов (т.е. для Bass, Congo и т.д.)
            trackList = new int[16];

            int key = instruments[i]; // Задаем клавишу, которая представляет инструмент (Bass, Hi-Hat и т.д.). Массив содержит MIDI-числа для каждого инструмента

            for (int j = 0; j < 16; j++) { // Делаем это для каждоготакта текущего ряда
                JCheckBox jc = (JCheckBox) checkBoxList.get(j + (16 * i));
                if (jc.isSelected()) { // Установлен ли флажок на этом такте? Если да, то помещаем значение клавиши в текущуюю ячейку массива
                    trackList[j] = key; // (ячейку, которая представляет такт).
                } else {                // Если нет, то инструмент не должен играть в этом такте,
                    trackList[j] = 0;   // поэтому присваиваем ему 0.
                }
            }

            makeTracks(trackList); // Для этого инструмента и для всех 16 тактов создаем события и добавляем их на дорожку.
            track.add(makeEvent(176, 1, 127, 0, 16));
        }
        track.add(makeEvent(192, 9, 1, 0, 15)); // Мы всегда должны быть уверены, что событие на такте 16 существует (они идут от 0 до 15). Иначе BeatBox может не пройти все 16 тактов, перед тем как заново начнет последовательность
        try {
            sequencer.setSequence(sequence);
            sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY); // Позволяет задать количество повторений цикла или, как в этом случае, непрерывный цикл
            sequencer.start();
            sequencer.setTempoInBPM(120);
        } catch (Exception e) {
            e.printStackTrace();
        } // Теперь мы проигрываем мелодию
    }

    public class MyStartListener implements ActionListener { // Первый из внутренних классов-слушателей для кнопок.

        public void actionPerformed(ActionEvent a) {
            buildTrackAndStart();
        }
    }

    public class MyStopListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            sequencer.stop();
        }
    }

    public class MyUpTempoListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            float tempoFactor = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float) (tempoFactor * 1.03)); // Коэффициент темпа
        }
    }

    public class MyDownTempoListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            float tempoFactor = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float) (tempoFactor * .97)); // Коэффициент темпа
        }
    }

    public void makeTracks(int[] list) { // Метод создает события для одного инструмента за каждый проход цикла для всех 16 тактов.
        // Можно поллучить int[] для Bass drum, и каждый элемент массива будет содержать либо клавишу
        // этого инструмента, либо 0. Если это 0, то инструмент не должен играть на текущем такте.
        // Иначе нужно создать событие и добавить его в дорожку.
        for (int i = 0; i < 16; i++) {
            int key = list[i];

            if (key != 0) {
                track.add(makeEvent(144, 9, key, 100, i));     // Создаем события включения и выключения
                track.add(makeEvent(128, 9, key, 100, i + 1));   // и добавляем их в дорожку
            }
        }
    }

    public MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return event;
    }

    public class MySendListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            boolean[] checkboxState = new boolean[256]; // Создаем булев массив для хранениясостояния каждого флажка
            for (int i = 0; i < 256; i++) {
                JCheckBox check = (JCheckBox) checkBoxList.get(i); // Пробегаем через checkBoxList (ArrayList, содержащий состояния флажков), считываем
                if (check.isSelected()) {                          // состояния и добавляем полученные значения в булев массив
                    checkboxState[i] = true;
                }
            }
            try {
                FileOutputStream fileStream = new FileOutputStream(new File("Checkbox.ser"));
                ObjectOutputStream os = new ObjectOutputStream(fileStream);
                os.writeObject(checkboxState);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public class MyReadInListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            boolean[] checkboxState = null;
            try {
                FileInputStream fileIn = new FileInputStream(new File("Checkbox.ser"));
                ObjectInputStream is = new ObjectInputStream(fileIn);
                checkboxState = (boolean[]) is.readObject(); // Считываем объект из файла и определяем его как булев массив
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            for (int i = 0; i < 256; i++) {
                JCheckBox check = (JCheckBox) checkBoxList.get(i); // Восстанавливаем состояние каждого флажка в ArrayList, содержащий объекты JCheckBox (checkBoxList)
                if (checkboxState[i]) {
                    check.setSelected(true);
                } else {
                    check.setSelected(false);
                }
            }
            sequencer.stop();      // Останавливаем проигрывание мелодии и восстанавливаем последовательность, используя новые состояния флажков в ArrayList
            buildTrackAndStart();
        }
    }
}
