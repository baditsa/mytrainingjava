package com.baditsa.Chapter16;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 15.04.15
 * Time: 14:33
 * To change this template use File | Settings | File Templates.
 */
public class TestGenerics1 {
    public static void main(String[] args) {
        new TestGenerics1().go();
    }

    public void go() {
        ArrayList<Animal> animals = new ArrayList<Animal>();
        animals.add(new Dog());
        animals.add(new Cat());
        animals.add(new Dog());
        takeAnimals(animals); // Вызываем takeAnimals(), используя в качестве аргументов массивы обоих типов
    }

    public void takeAnimals(ArrayList<Animal> animals) { // Метод takeAnimals() может принимать как Animal[], так и Dog[], т.к. Dog является Animal
        for (Animal a : animals) {
            a.eat();
        }
    }

    abstract class Animal {
        void eat() {
            System.out.println("животное ест");
        }
    }

    class Dog extends Animal {
        void bark() {
        }

        ;
    }

    class Cat extends Animal {
        void meow() {
        }

        ;
    }
}
