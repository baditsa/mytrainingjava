package com.baditsa.Chapter12;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 02.04.15
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
public class SimpleAnimation {

    int x = 70;
    int y = 70;

    public static void main(String[] args) {
        SimpleAnimation gui = new SimpleAnimation();
        gui.go();
    }

    public void go() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        MyDrawPanel drawPanel = new MyDrawPanel();

        frame.getContentPane().add(drawPanel);
        frame.setSize(300, 300);
        frame.setVisible(true);

        for (int i = 0; i < 130; i++) { // Повторяем 130 раз

            x++;
            y++;

            drawPanel.repaint(); // Говорим панели, чтобы она себя перерисовала

            try {
                Thread.sleep(50); // Немного замедляем процесс
            } catch (Exception ex) {
            }
        }
    }

    class MyDrawPanel extends JPanel {
        public void paintComponent(Graphics g) {
            g.setColor(Color.white);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            g.setColor(Color.green);
            g.fillOval(x, y, 40, 40); // Используем постоянно обновляющиеся координаты x и y внешнего класса
        }
    }
}
