package com.baditsa.Chapter12;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 31.03.15
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
public class SimpleGui1B implements ActionListener { // Реализуем интерфейс. Код говорит: "Экземпляр SimpleGui1B реализует интерфейс ActionListener"
    JButton button; // Кнопка будет передавать события только тем, кто реализует ActionListener

    public static void main(String[] args) {
        SimpleGui1B gui = new SimpleGui1B();
        gui.go();
    }

    public void go() {
        JFrame frame = new JFrame();
        button = new JButton("click me");

        button.addActionListener(this); // Регистрируем нашу заинтересованность в кнопке. Код говорит кнопке "Добавь меня к своему списку слушателей".
        // Передаваемый аргумент должен быть объектом класса, реализуемого ActionListener
        frame.getContentPane().add(button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent event) { // Реализуем метод actionPerformed() интерфейса ActionListener. Это фактический метод обработки событий
        button.setText("I've been clicked!"); // Кнопка вызывает этот метод, чтобы известить о наступлении события. Она отправляет объект ActionEvent как аргумент,
    }                                         // но он нам не нужен. Достаточно знать, что событие произошло
}
