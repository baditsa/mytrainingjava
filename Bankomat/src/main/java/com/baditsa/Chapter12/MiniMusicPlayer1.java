package com.baditsa.Chapter12;

import javax.sound.midi.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 02.04.15
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */
public class MiniMusicPlayer1 {
    public static void main(String[] args) {
        try {

            Sequencer sequencer = MidiSystem.getSequencer(); // Создаем
            sequencer.open(); // и открываемсинтезатор

            Sequence seq = new Sequence(Sequence.PPQ, 4); // Создаем последовательность
            Track track = seq.createTrack(); // и дорожку

            for (int i = 5; i < 61; i += 4) { // Создаем группу событий, чтобы ноты продолжали подниматься

                track.add(makeEvent(144, 1, i, 100, i)); // Вызываем новый метод makeEvent(), чтобы создать сообщение и событие
                track.add(makeEvent(128, 1, i, 100, i + 2));
            }

            sequencer.setSequence(seq);
            sequencer.setTempoInBPM(220);
            sequencer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        } catch (Exception e) {
        }
        return event;
    }
}
