package com.baditsa.Chapter12;

import javax.swing.*;


/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 31.03.15
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
//import javax.swing.*
public class SimpleGui1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JButton button = new JButton("click me");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Эта строка завершает работупрограммы при закрытии окна
        frame.getContentPane().add(button); // Добавляем кнопку на панель фрейма
        frame.setSize(300, 300); // Присваиваем фрейму размер (в пикселах)
        frame.setVisible(true); // Делаем фрейм видимым
    }
}

