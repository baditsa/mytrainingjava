package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.08.15
 * Time: 18:34
 * To change this template use File | Settings | File Templates.
 */
public class NoMoneyState implements State {
    Cashbox cashbox;

    public NoMoneyState(Cashbox cashbox) {
        this.cashbox = cashbox;
    }

    @Override
    public void insertMoney() {
        System.out.println("Вы внесли деньги.");
        cashbox.setState(cashbox.getHasMoneyState());
    }

    @Override
    public void ejectMoney() {
        System.out.println("Чтобы вернуть деньги их нужно сначала внести!");
    }

    @Override
    public void pay() {
        System.out.println("Чтобы оплатить билет необходимо сначала внести деньги!");
    }

    @Override
    public void giveTicket() {
        System.out.println("Билет выдается только по предоплате!");
    }
}