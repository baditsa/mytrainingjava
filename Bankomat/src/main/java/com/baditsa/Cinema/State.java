package com.baditsa.Cinema;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.08.15
 * Time: 16:40
 * To change this template use File | Settings | File Templates.
 */
public interface State extends Serializable {
    public void insertMoney();

    public void ejectMoney();

    public void pay();

    public void giveTicket();
}