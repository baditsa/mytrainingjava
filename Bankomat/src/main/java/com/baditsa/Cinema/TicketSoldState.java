package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.08.15
 * Time: 18:03
 * To change this template use File | Settings | File Templates.
 */
public class TicketSoldState implements State {
    Cashbox cashbox;

    public TicketSoldState(Cashbox cashbox) {
        this.cashbox = cashbox;
    }

    @Override
    public void insertMoney() {
        System.out.println("Подождите, Вам уже выдали билет!");
    }

    @Override
    public void ejectMoney() {
        System.out.println("Извините, но оплата билета уже произведена!");
    }

    @Override
    public void pay() {
        System.out.println("Это не поможет Вам получить второй билет бесплатно!");
    }

    @Override
    public void giveTicket() {
        cashbox.releaseTicket();
        if (cashbox.getCount() > 0) {
            cashbox.setState(cashbox.getNoMoneyState());
        } else {
            System.out.println("Билетов больше нет!");
            cashbox.setState(cashbox.getNoTicketsState());

        }
    }
}