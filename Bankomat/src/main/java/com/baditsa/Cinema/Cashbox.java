package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.08.15
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
public class Cashbox {
    State noTicketsState;
    State noMoneyState;
    State hasMoneyState;
    State ticketSoldState;

    State state = noTicketsState; // переменная экземпляра для хранения State
    int count = 0; // переменная экземпляра для хранения количества билетов

    public Cashbox(int numberTickets) { // Конструктор получает исходное количество билетов и сохраняет его в переменной
        noTicketsState = new NoTicketsState(this);
        noMoneyState = new NoMoneyState(this);
        hasMoneyState = new HasMoneyState(this);
        ticketSoldState = new TicketSoldState(this);
        this.count = numberTickets;
        if (numberTickets > 0) {
            state = noMoneyState;
        }
    }

    public void insertMoney() {
        state.insertMoney();
    }

    public void ejectMoney() {
        state.ejectMoney();
    }

    public void pay() {
        state.pay();
        state.giveTicket();
    }

    void setState(State state) {
        this.state = state;
    }

    void releaseTicket() {
        System.out.println("Получите свой билет!");
        if (count > 0) {
            count = count - 1;
        }
    }

    public State getHasMoneyState() {
        return hasMoneyState;
    }

    public State getNoMoneyState() {
        return noMoneyState;
    }

    public State getTicketSoldState() {
        return ticketSoldState;
    }

    public State getNoTicketsState() {
        return noTicketsState;
    }

    public int getCount() {
        return count;
    }
}