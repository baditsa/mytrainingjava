package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 02.09.15
 * Time: 18:53
 * To change this template use File | Settings | File Templates.
 */
public abstract class Films {
    String name = "Unknown film";

    public String getName() {
        return name;
    }

    public abstract int duration();
}