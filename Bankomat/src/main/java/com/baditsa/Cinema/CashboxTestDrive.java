package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 31.08.15
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
public class CashboxTestDrive {
    public static void main(String[] args) {
        Cashbox cashbox = new Cashbox(5);

        System.out.println(cashbox);

        cashbox.insertMoney();
        cashbox.pay();

        System.out.println(cashbox);
    }
}