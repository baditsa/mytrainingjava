package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 02.09.15
 * Time: 19:40
 * To change this template use File | Settings | File Templates.
 */
public class Transformers extends Films {
    public Transformers() {
        name = "Transformers";
    }

    @Override
    public int duration() {
        return 120;
    }
}
