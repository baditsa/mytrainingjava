package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.08.15
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
public class NoTicketsState implements State {
    Cashbox cashbox;

    public NoTicketsState(Cashbox cashbox) {
        this.cashbox = cashbox;
    }

    @Override
    public void insertMoney() {
        System.out.println("Вы не можете внести деньги - билетов нет.");
    }

    @Override
    public void ejectMoney() {
        System.out.println("Вы не можете забрать деньги - их нужно сначала внести.");
    }

    @Override
    public void pay() {
        System.out.println("Вы не можете оплатить билет, т.к. их нет.");
    }

    @Override
    public void giveTicket() {
        System.out.println("Билетов нет!");
    }
}