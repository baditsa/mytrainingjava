package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 02.09.15
 * Time: 19:35
 * To change this template use File | Settings | File Templates.
 */
public class Terminator extends Films {
    public Terminator() {
        name = "Terminator";
    }

    @Override
    public int duration() {
        return 90;
    }
}
