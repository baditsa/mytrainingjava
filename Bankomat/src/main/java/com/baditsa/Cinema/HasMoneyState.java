package com.baditsa.Cinema;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 28.08.15
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */
public class HasMoneyState implements State {
    Cashbox cashbox;

    public HasMoneyState(Cashbox cashbox) {
        this.cashbox = cashbox;
    }

    @Override
    public void insertMoney() {
        System.out.println("Вы уже внесли деньги!");
    }

    @Override
    public void ejectMoney() {
        System.out.println("Заберите Ваши деньги!");
        cashbox.setState(cashbox.getNoMoneyState());
    }

    @Override
    public void pay() {
        System.out.println("Оплата...");
        cashbox.setState(cashbox.getTicketSoldState());
    }

    @Override
    public void giveTicket() {
        System.out.println("Хрен Вам!");
    }
}