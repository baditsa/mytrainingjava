package com.baditsa.DuckTurkey;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class DuckTestDrive {
    public static void main(String[] args) {
        MallardDuck duck = new MallardDuck(); // create objects Duck
        WildTurkey turkey = new WildTurkey(); // and Turkey
        Duck turkeyAdapter = new TurkeyAdapter(turkey); // Turkey packed in TurkeyAdapter and starting to look like a Duck

        System.out.println("The Turkey says...");
        turkey.gobble();
        turkey.fly();

        System.out.println("\nThe Duck says...");
        testDuck(duck);

        System.out.println("\nThe TurkeyAdapter says...");
        testDuck(turkeyAdapter);
    }

    static void testDuck(Duck duck) {
        duck.quack();
        duck.fly();
    }
}