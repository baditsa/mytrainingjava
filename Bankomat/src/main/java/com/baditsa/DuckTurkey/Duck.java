package com.baditsa.DuckTurkey;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */
public interface Duck {
    public void quack();

    public void fly();
}
