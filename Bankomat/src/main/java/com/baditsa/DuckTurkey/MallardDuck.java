package com.baditsa.DuckTurkey;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 14:17
 * To change this template use File | Settings | File Templates.
 */
public class MallardDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("Quack");
    }

    @Override
    public void fly() {
        System.out.println("I'm flying");
    }
}
