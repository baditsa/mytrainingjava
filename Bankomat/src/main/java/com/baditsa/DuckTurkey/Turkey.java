package com.baditsa.DuckTurkey;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
public interface Turkey {
    public void gobble();

    public void fly();
}
