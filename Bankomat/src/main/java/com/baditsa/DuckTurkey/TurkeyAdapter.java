package com.baditsa.DuckTurkey;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public class TurkeyAdapter implements Duck { // first of all necessary to implement interface of the type which is expected for your client
    Turkey turkey;

    public TurkeyAdapter(Turkey turkey) { // Then we get the link to the adapted object (in constructor)
        this.turkey = turkey;
    }

    @Override
    public void quack() {
        turkey.gobble();
    }

    @Override
    public void fly() {
        for (int i = 0; i < 5; i++) {
            turkey.fly();
        }
    }
}