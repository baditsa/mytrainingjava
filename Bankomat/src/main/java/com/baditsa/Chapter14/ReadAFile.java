package com.baditsa.Chapter14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 09.04.15
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */
public class ReadAFile {
    public static void main(String[] args) {

        try {
            File myFile = new File("MyText.txt");
            FileReader fileReader = new FileReader(myFile); // FileReader - поток соединения для символов, который подключается к текстовому файлу
            BufferedReader reader = new BufferedReader(fileReader); // Для более эффективного чтения соединим FileReader с BufferedReader. Тогда FileReader будет обращаться
            // к файлу только в том случае, если буфер будет пуст
            String line = null;
            while ((line = reader.readLine()) != null) { // Пока здесь есть строки для чтения, считывай их
                System.out.println(line); // и выводи на экран
            }
            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
