package com.baditsa.Chapter14;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 08.04.15
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
public class Box implements Serializable {

    private int width;
    private int height;

    public void setWidth(int w) {
        width = w;
    }

    public void setHeight(int h) {
        height = h;
    }

    public static void main(String[] args) {
        Box myBox = new Box();
        myBox.setWidth(50);
        myBox.setHeight(20);

        try {
            FileOutputStream fs = new FileOutputStream("foo.ser"); // Соединяемся с файлом с именем foo.ser, если он существует.
            // В ином случае будет создан новый файл с именем foo.ser.
            ObjectOutputStream os = new ObjectOutputStream(fs);    // Связываем ObjectOutputStream с потоком соединения.
            // Приказываем ему записать объект
            os.writeObject(myBox); // Сериализируем объекты, на которые указывает ссылка myBox и записываем его в файл foo.ser
            os.close(); // Закрываем ObjectOutputStream
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
