package com.baditsa.GumBallMachine;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 27.07.15
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
public class MyRemoteImpl extends UnicastRemoteObject implements MyRemote { // Расширение UnicastRemoteObject - простейший способ создания объекта с поддержкой удаленного доступа

    // Обязательно реализуем итерфейс удаленного доступа
    @Override
    public String sayHello() { // Реализуем методы интерфейса
        return "Server says, Hey";
    }

    public MyRemoteImpl() throws RemoteException {
    } // Конструктор суперкласса (UnicastRemoteObject)

    public static void main(String[] args) {
        try {
            MyRemote service = new MyRemoteImpl(); // Создаем удаленный объект и
            Naming.rebind("RemoteHello", service); // регистрируем его в реестре RMI статическим вызовом Naming.rebind(). Регистрируемое имя будет использоваться клиентами для поиска объекта в реестре
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}