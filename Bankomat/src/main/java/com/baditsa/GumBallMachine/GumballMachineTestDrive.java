package com.baditsa.GumBallMachine;

import java.rmi.Naming;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 22.07.15
 * Time: 18:30
 * To change this template use File | Settings | File Templates.
 */
public class GumballMachineTestDrive {
    public static void main(String[] args) {
        GumballMachineRemote gumballMachine = null;
        int count;

        if (args.length < 2) {
            System.out.println("GumballMachine <name> <inventory>"); // Местонахождение и исходное количество шариков передаются в командной строке
            System.exit(1);
        }

        try {
            count = Integer.parseInt(args[1]);

            gumballMachine = new GumballMachine(args[0], count); // Передаем параметры конструктору
            Naming.rebind("//" + args[0] + "/gumballMachine", gumballMachine);

            // GumballMonitor monitor = new GumballMonitor(gumballMachine); // Создание монитора с указанием автомата, для которого строится отчет

            // monitor.report();





        /*GumballMachine gumballMachine = new GumballMachine(5); // Загружаем 5 шариков

        System.out.println(gumballMachine); // Выводим состояние автомата

        gumballMachine.insertQuarter(); // Бросаем монетку
        gumballMachine.turnCrank(); // Дёргаем за рычаг

        System.out.println(gumballMachine); // Снова выводим состояние

      *//*gumballMachine.insertQuarter(); // Бросаем монетку
        gumballMachine.ejectQuarter(); // Требуем её обратно
        gumballMachine.turnCrank(); // Дёргаем за рычаг - автомат не должен выдать шарик

        System.out.println(gumballMachine); // И снова выводим состояние автомата*//*

        gumballMachine.insertQuarter(); // Бросаем монетку
        gumballMachine.turnCrank(); // Дёргаем за рычаг - автомат должен выдать шарик
        gumballMachine.insertQuarter(); // Бросаем монетку
        gumballMachine.turnCrank(); // Дёргаем за рычаг - автомат должен выдать шарик
      //gumballMachine.ejectQuarter(); // Требуем вернуть монетку, которую не бросали

        System.out.println(gumballMachine); // Снова выводим состояние автомата

      *//*gumballMachine.insertQuarter(); // Бросаем
        gumballMachine.insertQuarter(); //         две монетки
        gumballMachine.turnCrank(); // Дёргаем за рычаг - автомат должен выдать шарик
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println(gumballMachine);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}