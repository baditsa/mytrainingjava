package com.baditsa.GumBallMachine;

import java.rmi.Naming;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 27.07.15
 * Time: 17:11
 * To change this template use File | Settings | File Templates.
 */
public class GumballMonitorTestDrive {

    public static void main(String[] args) {
        // Создаем массив местонахождений автоматов
        String[] location = {"rmi://santafe.mightygumball.com/gumballmachine",
                "rmi://boulder.mightygumball.com/gumballmachine",
                "rmi://seattle.mightygumball.com/gumballmachine"};

        // Создаем массив мониторов
        GumballMonitor[] monitor = new GumballMonitor[location.length];

        for (int i = 0; i < location.length; i++) {
            try {
                // Нужно получить заместителя для каждого автомата
                // Naming.lookup() - статический метод из пакета RMI, который ищет службу в реестре RMI по заданному местонахождению и имени
                GumballMachineRemote machine = (GumballMachineRemote) Naming.lookup(location[i]);
                // Получив заместителя для удаленного автомата, мы создаем новый объект GumballMonitor и передаем ему отслеживаемый автомат
                monitor[i] = new GumballMonitor(machine);
                System.out.println(monitor[i]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Перебираем автоматы и для каждого выводим отчет
        for (int i = 0; i < monitor.length; i++) {
            monitor[i].report();
        }
    }
}