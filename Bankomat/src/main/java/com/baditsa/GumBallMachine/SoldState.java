package com.baditsa.GumBallMachine;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 24.07.15
 * Time: 14:07
 * To change this template use File | Settings | File Templates.
 */
public class SoldState implements State {
    transient GumballMachine gumballMachine;

    public SoldState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("Please wait, we're already giving you a gumball");
    }

    @Override
    public void ejectQuarter() {
        System.out.println("Sorry, you already turned the crank");
    }

    @Override
    public void turnCrank() {
        System.out.println("Turning twice doesn't get you another gumball!");
    }

    @Override
    public void dispense() {
        gumballMachine.releaseBall(); // Находимся в состоянии SoldState (покупка оплачена). Сначала приказываем автомату выдать шарик
        if (gumballMachine.getCount() > 0) { // Затем проверяем количество шариков и, в зависимости от результата, переходим в состояние NoQuarterState или SoldOutState()
            gumballMachine.setState(gumballMachine.getNoQuarterState());
        } else {
            System.out.println("Oops, out of gumballs!");
            gumballMachine.setState(gumballMachine.getSoldOutState());
        }
    }
}