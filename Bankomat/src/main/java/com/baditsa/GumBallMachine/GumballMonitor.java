package com.baditsa.GumBallMachine;

import java.rmi.RemoteException;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 24.07.15
 * Time: 18:03
 * To change this template use File | Settings | File Templates.
 */
public class GumballMonitor {
    GumballMachineRemote machine;

    public GumballMonitor(GumballMachineRemote machine) { // Конструктор получает объект автомата
        this.machine = machine;                           // и сохраняет его в переменной экземпляра machine
    }

    public void report() {
        try {
            System.out.println("Gumball Machine: " + machine.getLocation());
            System.out.println("Current inventory: " + machine.getCount() + " gumballs");
            System.out.println("Current state: " + machine.getState());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}