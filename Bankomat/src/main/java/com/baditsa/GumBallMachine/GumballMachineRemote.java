package com.baditsa.GumBallMachine;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 27.07.15
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
public interface GumballMachineRemote extends Remote {
    // интерфейс удаленного доступа
    public int getCount() throws RemoteException;

    public String getLocation() throws RemoteException;

    public State getState() throws RemoteException;
}