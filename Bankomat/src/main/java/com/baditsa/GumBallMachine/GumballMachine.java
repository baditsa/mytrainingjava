package com.baditsa.GumBallMachine;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 22.07.15
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class GumballMachine extends UnicastRemoteObject implements GumballMachineRemote {

    State soldOutState;
    State noQuarterState;
    State hasQuarterState;
    State soldState;
    State winnerState;

    String location;

    State state = soldOutState; // Переменная экземпляра для хранения Actions
    int count = 0; // для хранения кол-ва шариков. Изначально автомат пуст

    public GumballMachine(String location, int count) throws RemoteException { // Конструктор получает исходное количество шариков и сохраняет его в переменной
        soldOutState = new SoldOutState(this);
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        soldState = new SoldState(this);
        winnerState = new WinnerState(this);
        this.location = location;
        this.count = count;
        if (count > 0) {
            state = noQuarterState;
        }
    }

    public void insertQuarter() {
        state.insertQuarter();
    }

    public void ejectQuarter() {
        state.ejectQuarter();
    }

    public void turnCrank() { // Для метода dispense() в классе GumballMachine метод действия не нужен, т.к. это
        state.turnCrank();    // внутреннее действие. Пользователь не может напрямую потребовать, чтобы автомат
        state.dispense();     // выдал шарик. Однако метод dispense() для объекта Actions вызывается из метода turnCrank()
    }

    void setState(State state) { // Метод setState() позволяет другим объектам (в частности, нашим объектам Actions)
        this.state = state;      // перевести автомат в другое состояние
    }

    void releaseBall() {
        System.out.println("A gumball comes rolling out the slot");
        if (count != 0) {      // Вспомогательный метод releaseBall() отпускает
            count = count - 1; // шарик  и уменьшает значение переменной count
        }
    }

    public State getHasQuarterState() {
        return hasQuarterState;
    }

    public State getNoQuarterState() {
        return noQuarterState;
    }

    public State getSoldState() {
        return soldState;
    }

    public State getWinnerState() {
        return winnerState;
    }

    public State getSoldOutState() {
        return soldOutState;
    }

    public String getLocation() {
        return location;
    }

    public int getCount() {
        return count;
    }

    public State getState() {
        return state;
    }


//----------------------------------------------------------------------------------------------------------------------
  /*final static int SOLD_OUT = 0;     // Нет шариков
    final static int NO_QUARTER = 1;   // Нет монетки
    final static int HAS_QUARTER = 2;  // Есть монетка
    final static int SOLD = 3;         // Шарик продан

    int state = SOLD_OUT;              // Переменная экземпляра, в которой будет храниться текущее состояние
    int count = 0;                     // Во второй переменной хранится количество шариков в автомате

    public GumballMachine(int count) { // Конструктор получает исходное количество шариковю Если оно отлично от нуля -
        this.count = count;            // автомат переходит в состояние NO_QUARTER, ожидая, пока кто-нибудь бросит
        if (count > 0) {               // монетку. В противном случае автомат останется в состоянии SOLD_OUT.
            state = NO_QUARTER;
        }
    }
//-------------------------------------------- В автомат бросают монетку -----------------------------------------------
    public void insertQuarter() {
        if (state == HAS_QUARTER) {       // В автомате уже есть монетка,
            System.out.println("You can't insert another quarter"); // сообщаем об этом покупателю.
        } else if (state == NO_QUARTER) { // В противном случае автомат принимает монетку
            state = HAS_QUARTER;          // и переходит в состояние HAS_QUARTER.
            System.out.println("You inserted a quarter");
        } else if (state == SOLD_OUT) {   // А если все шарики распроданы - автомат отклоняет монетку
            System.out.println("You can't insert a quarter, the machine is sold out");
        } else if (state == SOLD) {       // Если шарик был куплен - следует подождать завершения операции, прежде чем бросать другую монетку
            System.out.println("Please wait, we're already giving you a gumball");
        }
    }
//------------------------------------------- Покупатель пытается вернуть монетку --------------------------------------
    public void ejectQuarter() {
        if (state == HAS_QUARTER) {                 // Если монетка есть - автомат
            System.out.println("Quarter returned"); // вовращает её
            state = NO_QUARTER;                     // и переходит в состояние NO_QUARTER.
        } else if (state == NO_QUARTER) {           // Если монетки нет - вернуть её невозможно.
            System.out.println("You haven't inserted a quarter");
        } else if (state == SOLD) {                 // Шарик уже куплен - возврат невозможен
            System.out.println("Sorry, you already turned the crank");
        } else if (state == SOLD_OUT) {             // Если шарики кончились - возврат невозможен, автомат не принимает монетки
            System.out.println("You can't eject, you haven't inserted a quarter yet");
        }
    }
//------------------------------------------- Покупатель пытается дернуть за рычаг -------------------------------------
    public void turnCrank() {
        if (state == SOLD) {
            System.out.println("Turning twice doesn't get you another gumball!");
        } else if (state == NO_QUARTER) {
            System.out.println("You turned, but there's no quarter");
        } else if (state == SOLD_OUT) {
            System.out.println("You turned, but there are no gumballs");
        } else if (state == HAS_QUARTER) {
            System.out.println("You turned...");
            state = SOLD;
            dispense();
        }
    }
//------------------------------------------------ Метод для выдачи шарика ---------------------------------------------
    public void dispense() {
        if (state == SOLD) {
            System.out.println("A gumball comes rolling out the slot");
            count = count - 1;
            if (count == 0) {                                 // Если шарик был последним -
                System.out.println("Oops, out of gumballs!"); // автомат переходит
                state = SOLD_OUT;                             // в состояние SOLD_OUT.
            } else {                                          // А если нет - возвращается
                state = NO_QUARTER;                           // к состоянию NO_QUARTER
            }
        } else if (state == NO_QUARTER) {
            System.out.println("You need to pay first");
        } else if (state == SOLD_OUT) {
            System.out.println("No gumball dispensed");
        } else if (state == HAS_QUARTER) {
            System.out.println("No gumball dispensed");
        }
    }*/
}