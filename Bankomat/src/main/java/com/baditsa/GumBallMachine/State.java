package com.baditsa.GumBallMachine;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 23.07.15
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
public interface State extends Serializable {
    public void insertQuarter();

    public void ejectQuarter();

    public void turnCrank();

    public void dispense();
}