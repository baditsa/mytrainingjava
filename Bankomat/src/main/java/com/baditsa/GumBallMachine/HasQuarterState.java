package com.baditsa.GumBallMachine;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 24.07.15
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
public class HasQuarterState implements State {
    Random randomWinner = new Random(System.currentTimeMillis()); // Генератор случайных чисел с 10% вероятностью выигрыша
    transient GumballMachine gumballMachine;

    public HasQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("You can't insert another quarter"); // Некорректное действие для этого состояния
    }

    @Override
    public void ejectQuarter() {
        System.out.println("Quarter returned");                      // Вернуть монетку и
        gumballMachine.setState(gumballMachine.getNoQuarterState()); // перейти в состояние NoQuarterState
    }

    @Override
    public void turnCrank() {
        System.out.println("You turned..."); // Когда покупатель дергает за рычаг, автомат переводится в состояние SoldState.
        int winner = randomWinner.nextInt(10);
        if ((winner == 0) && (gumballMachine.getCount() > 1)) { // Если в автомате остался второй шарик - переходим в состояние WinnerState.
            gumballMachine.setState(gumballMachine.getWinnerState()); // переходим в состояние WinnerState.
        } else { // Если нет - в состояние SoldState
            gumballMachine.setState(gumballMachine.getSoldState()); // Для получения объекта SoldState используется метод getSoldState().
        }
    }

    @Override
    public void dispense() {
        System.out.println("No gumball dispensed"); // Другое некорректное действие для этого состояния
    }
}
