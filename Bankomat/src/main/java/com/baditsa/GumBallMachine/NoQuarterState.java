package com.baditsa.GumBallMachine;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 23.07.15
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public class NoQuarterState implements State {
    transient GumballMachine gumballMachine;

    public NoQuarterState(GumballMachine gumballMachine) { // Конструктору передается ссылка на объект автомата,
        this.gumballMachine = gumballMachine;              // которая просто сохраняется в переменной экземпляра
    }

    @Override
    public void insertQuarter() {
        System.out.println("You inserted a quarter");
        gumballMachine.setState(gumballMachine.getHasQuarterState());
    }

    @Override
    public void ejectQuarter() {
        System.out.println("You haven't inserted a quarter");
    }

    @Override
    public void turnCrank() {
        System.out.println("You turned, but there's no quarter");
    }

    @Override
    public void dispense() {
        System.out.println("You need to pay first");
    }
}