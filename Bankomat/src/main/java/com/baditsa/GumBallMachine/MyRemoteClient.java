package com.baditsa.GumBallMachine;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 27.07.15
 * Time: 14:54
 * To change this template use File | Settings | File Templates.
 */
public class MyRemoteClient {
    public static void main(String[] args) {
        new MyRemoteClient().go();
    }

    private void go() {
        try {

            MyRemote service = (MyRemote) Naming.lookup("rmi: //127.0.0.1/RemoteHello");

            String s = service.sayHello();

            System.out.println(s);

        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }
}