package com.baditsa.GumBallMachine;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 27.07.15
 * Time: 14:06
 * To change this template use File | Settings | File Templates.
 */
public interface MyRemote extends Remote {
    public String sayHello() throws RemoteException; // Все удаленные методы должны объявлять RemoteException
}
