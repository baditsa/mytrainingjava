package com.baditsa.EnumerationIterator;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
public interface Enumeration {
    public boolean hasMoreElements();

    public void nextElement();
}
