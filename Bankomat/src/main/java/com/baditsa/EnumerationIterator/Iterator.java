package com.baditsa.EnumerationIterator;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 14.07.15
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
public interface Iterator {
    public boolean hasNext();

    public Object next();

    public void remove();
}