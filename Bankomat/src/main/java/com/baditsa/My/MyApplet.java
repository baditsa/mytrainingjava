package com.baditsa.My;

import java.applet.Applet;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 17.07.15
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
public class MyApplet extends Applet {
    String message;

    public void init() {
        message = "Hello World!";
        repaint();
    }

    public void start() {
        message = "Now I'm starting...";
        repaint();
    }

    public void stop() {
        message = "Now I'm being stopped...";
        repaint();
    }

    public void destroy() {
    }

    public void paint(Graphics g) {
        g.drawString(message, 5, 15);
    }
}