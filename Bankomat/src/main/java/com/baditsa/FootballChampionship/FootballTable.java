package com.baditsa.FootballChampionship;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 30.06.15
 * Time: 14:23
 * To change this template use File | Settings | File Templates.
 */
// турнирная таблица по футболу
public class FootballTable extends JFrame implements ActionListener {

    // компоненты формы
    private JPanel mainPanel, infoPanel, tablePanel, matchPanel;
    private JButton jbtnDisplayTeamInfo, jbtnEPLTable, jbtnMatchResult, jbtnSaveTeamInfo, jbtnSetResult;
    private JLabel jlblAwayTeamCaption, jlblResultCaption, jlblEPLTableCaption, jlblHomeTeamCaption, jlblMatchResultCaption, jlblTeamInfoCaption;
    private JList jlstAwayTeam, jlstHomeTeam;
    private JTable jtblTeamInfo, jtblEPL;
    private JTextField jtxtMatchResult;
    private JScrollPane spAway, spEPLTable, spHome, spTeamInfo;

    // модели для таблиц и списков
    private DefaultTableModel teamInfoModel, eplTableModel;
    private DefaultListModel homeTeamModel, awayTeamModel;

    // константы для панелей
    private final String INFO = "infoPanel"; // панель с информацией о командах
    private final String MATCH = "matchPanel"; // панель для ввода результатов матча
    private final String TABLE = "tablePanel"; // турнирная таблица

    // путь к файлу
    private final String FILEPATH = FootballTable.class.getClassLoader().getResource("premiership.txt").getFile();
    ;

    // коллекция
    ArrayList<TeamSummary> teams = new ArrayList<TeamSummary>();

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FootballTable().setVisible(true);
            }
        });
    }

    //----------------------------------------------------------------------------------------------------------------------
    // конструктор
    public FootballTable() {
        // создание графических компонентов
        initComponents();

        // загрузка данных из файла в список
        loadFile(FILEPATH);

        // заполнение таблиц
        populateInfoTable();
        populateEPLTable();

        // заполнение листов
        populateLists();
    }

    //--------------------------------------------создание графических компонентов------------------------------------------
    private void initComponents() {
        // панели
        mainPanel = new JPanel();
        infoPanel = new JPanel();
        tablePanel = new JPanel();
        matchPanel = new JPanel();

        mainPanel.setBackground(new Color(238, 232, 231));
        infoPanel.setBackground(new Color(204, 204, 255));
        tablePanel.setBackground(new Color(204, 235, 204));
        matchPanel.setBackground(new Color(196, 215, 255));

        mainPanel.setLayout(new CardLayout());
        mainPanel.add(infoPanel, INFO);
        mainPanel.add(tablePanel, TABLE);
        mainPanel.add(matchPanel, MATCH);

        // метки
        jlblAwayTeamCaption = new JLabel("Выберите вторую команду");
        jlblResultCaption = new JLabel("Результаты матча (голы:голы)");
        jlblEPLTableCaption = new JLabel("Таблица результатов");
        jlblHomeTeamCaption = new JLabel("Выберите первую команду");
        jlblMatchResultCaption = new JLabel("Результат матча");
        jlblTeamInfoCaption = new JLabel("Информация о командах");

        jlblTeamInfoCaption.setFont(new Font("Arial Black", 1, 18));
        jlblTeamInfoCaption.setHorizontalAlignment(SwingConstants.CENTER);

        jlblEPLTableCaption.setFont(new Font("Arial Black", 1, 18));
        jlblEPLTableCaption.setHorizontalAlignment(SwingConstants.CENTER);

        jlblMatchResultCaption.setFont(new Font("Arial Black", 1, 18));
        jlblMatchResultCaption.setHorizontalAlignment(SwingConstants.CENTER);

        jlblHomeTeamCaption.setFont(new Font("Tahoma", 1, 11));
        jlblAwayTeamCaption.setFont(new Font("Tahoma", 1, 11));
        jlblResultCaption.setFont(new Font("Tahoma", 1, 11));

        // текстовое поле
        jtxtMatchResult = new JTextField();

        // кнопки
        jbtnDisplayTeamInfo = new JButton("Информация о команде");
        jbtnEPLTable = new JButton("Таблица");
        jbtnMatchResult = new JButton("Результаты");
        jbtnSaveTeamInfo = new JButton("Сохранить в файл");
        jbtnSetResult = new JButton("Записать результат");

        jbtnDisplayTeamInfo.addActionListener(this);
        jbtnEPLTable.addActionListener(this);
        jbtnMatchResult.addActionListener(this);
        jbtnSaveTeamInfo.addActionListener(this);
        jbtnSetResult.addActionListener(this);

        // таблицы
        // модели для таблиц
        teamInfoModel = new DefaultTableModel();
        eplTableModel = new DefaultTableModel();
        // установить модели для таблиц
        jtblTeamInfo = new JTable(teamInfoModel);
        jtblEPL = new JTable(eplTableModel);

        // списки
        // модели для списков
        homeTeamModel = new DefaultListModel();
        awayTeamModel = new DefaultListModel();
        // установить модели для списков
        jlstAwayTeam = new JList(awayTeamModel);
        jlstHomeTeam = new JList(homeTeamModel);

        // панели прокрутки
        spAway = new JScrollPane();
        spEPLTable = new JScrollPane();
        spHome = new JScrollPane();
        spTeamInfo = new JScrollPane();

        spTeamInfo.setViewportView(jtblTeamInfo);
        spEPLTable.setViewportView(jtblEPL);
        spHome.setViewportView(jlstHomeTeam);
        spAway.setViewportView(jlstAwayTeam);

        // панель с информацией о командах
        GroupLayout infoPanelLayout = new GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(infoPanelLayout.createSequentialGroup().addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(infoPanelLayout.createSequentialGroup().addGap(26, 26, 26).addComponent(spTeamInfo, GroupLayout.PREFERRED_SIZE, 416, GroupLayout.PREFERRED_SIZE)).addGroup(infoPanelLayout.createSequentialGroup().addGap(110, 110, 110).addComponent(jlblTeamInfoCaption))).addContainerGap(23, Short.MAX_VALUE)));
        infoPanelLayout.setVerticalGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(infoPanelLayout.createSequentialGroup().addGap(13, 13, 13).addComponent(jlblTeamInfoCaption).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(spTeamInfo, GroupLayout.PREFERRED_SIZE, 337, GroupLayout.PREFERRED_SIZE).addContainerGap(19, Short.MAX_VALUE)));

        // турнирная таблица
        GroupLayout tablePanelLayout = new GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(tablePanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(tablePanelLayout.createSequentialGroup().addGroup(tablePanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(tablePanelLayout.createSequentialGroup().addGap(26, 26, 26).addComponent(spEPLTable, GroupLayout.PREFERRED_SIZE, 410, GroupLayout.PREFERRED_SIZE)).addGroup(tablePanelLayout.createSequentialGroup().addGap(128, 128, 128).addComponent(jlblEPLTableCaption))).addContainerGap(29, Short.MAX_VALUE)));
        tablePanelLayout.setVerticalGroup(tablePanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(tablePanelLayout.createSequentialGroup().addGap(13, 13, 13).addComponent(jlblEPLTableCaption).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(spEPLTable, GroupLayout.PREFERRED_SIZE, 337, GroupLayout.PREFERRED_SIZE).addContainerGap(19, Short.MAX_VALUE)));

        // панель для ввода результатов матча
        GroupLayout matchPanelLayout = new GroupLayout(matchPanel);
        matchPanel.setLayout(matchPanelLayout);
        matchPanelLayout.setHorizontalGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(matchPanelLayout.createSequentialGroup().addGap(31, 31, 31).addGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jlblHomeTeamCaption).addComponent(spHome, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE).addComponent(jlblResultCaption, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(matchPanelLayout.createSequentialGroup().addComponent(spAway, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE).addContainerGap()).addGroup(matchPanelLayout.createSequentialGroup().addGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(matchPanelLayout.createSequentialGroup().addComponent(jtxtMatchResult, GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18).addComponent(jbtnSetResult)).addComponent(jlblAwayTeamCaption, GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)).addContainerGap()))).addGroup(matchPanelLayout.createSequentialGroup().addGap(157, 157, 157).addComponent(jlblMatchResultCaption).addContainerGap(166, Short.MAX_VALUE)));
        matchPanelLayout.setVerticalGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(matchPanelLayout.createSequentialGroup().addGap(20, 20, 20).addComponent(jlblMatchResultCaption).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jlblHomeTeamCaption).addComponent(jlblAwayTeamCaption)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(spHome, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE).addComponent(spAway, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)).addGap(9, 9, 9).addGroup(matchPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jlblResultCaption).addComponent(jtxtMatchResult, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jbtnSetResult)).addContainerGap(192, Short.MAX_VALUE)));

        // менеджер расположения для главной панели фрейма
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(jbtnDisplayTeamInfo).addGap(18, 18, 18).addComponent(jbtnEPLTable).addGap(18, 18, 18).addComponent(jbtnMatchResult).addGap(18, 18, 18).addComponent(jbtnSaveTeamInfo).addGap(37, 37, 37)).addComponent(mainPanel, GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jbtnDisplayTeamInfo).addComponent(jbtnEPLTable).addComponent(jbtnMatchResult).addComponent(jbtnSaveTeamInfo)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(mainPanel, GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)));

        // свойства фрейма
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Чемпионат по футболу");
        setResizable(true);
        setLocationRelativeTo(null);
        pack();
    }

    //--------------------------------------------------загрузка данных из файла в список-----------------------------------
    private void loadFile(String filePath) {
        // определяем файл
        File file = new File(filePath);

        // объекты для считывания файла
        FileReader inputStream = null;
        BufferedReader inputBuffer = null;
        Scanner inputScanner = null;

        String line = "";

        String team;
        int played;
        int scored;
        int conceded;
        int points;

        // если файл существует
        if (file.exists()) {
            try { // работа с файлами - всегда в блоке try-catch
                inputStream = new FileReader(file);
                inputBuffer = new BufferedReader(inputStream);
                inputScanner = new Scanner(inputBuffer);

                // считать все строки
                while (inputScanner.hasNextLine()) {

                    // считывать построчно
                    line = inputScanner.nextLine();

                    // парсинг строки на поля
                    team = line.split(";")[0];
                    played = Integer.parseInt(line.split(";")[1]);
                    scored = Integer.parseInt(line.split(";")[2]);
                    conceded = Integer.parseInt(line.split(";")[3]);
                    points = Integer.parseInt(line.split(";")[4]);

                    // создание объекта и добавление в список
                    teams.add(new TeamSummary(team, played, scored, conceded, points));
                }
                // закрыть сканер, буфер и поток
                inputScanner.close();
                inputBuffer.close();
                inputStream.close();
            } catch (IOException e) { // ошибка ввода/вывода
                JOptionPane.showMessageDialog(this, e.getMessage(), "Ошибка ввода/вывода", JOptionPane.WARNING_MESSAGE);
            } catch (Exception e) { // остальные ошибки
                JOptionPane.showMessageDialog(this, e.getMessage(), "Ошибка", JOptionPane.WARNING_MESSAGE);
            }
        } else { // ошибка "Файл не существует"
            JOptionPane.showMessageDialog(this, "Файл не существует: \n" + filePath, "Ошибка", JOptionPane.WARNING_MESSAGE);
        }
    }

    //--------------------------------------------------заполнение таблиц---------------------------------------------------
    private void populateInfoTable() {
        String team;
        int played;
        int scored;
        int conceded;
        int points;

        // если нет столбцов - то добавить их
        if (teamInfoModel.getColumnCount() == 0) {
            teamInfoModel.addColumn("Команда");
            teamInfoModel.addColumn("Игры");
            teamInfoModel.addColumn("Голы");
            teamInfoModel.addColumn("Поражения");
            teamInfoModel.addColumn("Очки");
        }
        // очистить все данные таблицы
        if (teamInfoModel.getRowCount() > 0) {
            for (int i = teamInfoModel.getRowCount() - 1; i >= 0; i--) {
                teamInfoModel.removeRow(i);
            }
        }
        // создание строк с командами
        for (int i = 0; i < teams.size(); i++) {

            // получить значения
            team = teams.get(i).getTeamName();
            played = teams.get(i).getGamesPlayed();
            scored = teams.get(i).getGoalsScored();
            conceded = teams.get(i).getGoalsConceded();
            points = teams.get(i).getPoints();

            // добавляем в модель новую строку
            teamInfoModel.addRow(new Object[]{team, played, scored, conceded, points});
        }
    }

    private void populateEPLTable() {
        // сортировка списка
        ArrayList<TeamSummary> sortedTeams = getSortedTeams();

        int position;
        String team;
        int played;
        int goalsDifference;
        int points;

        // если нет столбцов - то добавить их
        if (eplTableModel.getColumnCount() == 0) {
            eplTableModel.addColumn("Позиция");
            eplTableModel.addColumn("Команда");
            eplTableModel.addColumn("Игры");
            eplTableModel.addColumn("Разница голов");
            eplTableModel.addColumn("Очки");
        }
        // очистить все данные таблицы
        if (eplTableModel.getRowCount() > 0) {
            for (int i = eplTableModel.getRowCount() - 1; i >= 0; i--) {
                eplTableModel.removeRow(i);
            }
        }
        // создание строк с командами
        for (int i = 0; i < sortedTeams.size(); i++) {

            // получить данные полей
            position = i + 1;
            team = sortedTeams.get(i).getTeamName();
            played = sortedTeams.get(i).getGamesPlayed();
            goalsDifference = sortedTeams.get(i).getGoalsDifference();
            points = sortedTeams.get(i).getPoints();

            // добавляем в модель новую строку
            eplTableModel.addRow(new Object[]{position, team, played, goalsDifference, points});
        }
    }

    //---------------------------------------------------заполнение листов--------------------------------------------------
    private void populateLists() {
        String team; // хранит название команды

        // очищаем все данные в моделях
        homeTeamModel.clear();
        awayTeamModel.clear();

        // заполняем имена команд
        for (int i = 0; i < teams.size(); i++) {
            team = teams.get(i).getTeamName();

            homeTeamModel.addElement(team);
            awayTeamModel.addElement(team);
        }
    }

    //-------------------------------------------------определение нажатой кнопки-------------------------------------------
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbtnDisplayTeamInfo) {
            displayCard(INFO); // показать информацию о команде
        } else if (e.getSource() == jbtnEPLTable) {
            displayCard(TABLE); // показать турнирную таблицу
        } else if (e.getSource() == jbtnMatchResult) {
            displayCard(MATCH); // показать панель для ввода результатов матча
        } else if (e.getSource() == jbtnSaveTeamInfo) {
            saveFile(FILEPATH); // сохранить таблицу в файл
        } else if (e.getSource() == jbtnSetResult) {
            processMatchResult(); // сохранить результаты в таблицу
        }
    }

    //-----------------------------------------------------сортировка списка------------------------------------------------
    private ArrayList<TeamSummary> getSortedTeams() {
        // создать копию
        ArrayList<TeamSummary> copy = new ArrayList<TeamSummary>();

        // скопировать все команды в копию
        TeamSummary objTeam = null;
        for (int i = 0; i < teams.size(); i++) {
            objTeam = teams.get(i);
            copy.add(objTeam);
        }
        // сортировка
        Collections.sort(copy, Collections.reverseOrder());

        // возвратить отсортированный список
        return copy;
    }

    //-----------------------------------------------показать нужную панель-------------------------------------------------
    private void displayCard(String cardName) {
        CardLayout cl = (CardLayout) (mainPanel.getLayout());
        cl.show(mainPanel, cardName);
    }

    //----------------------------------------------сохранение списка в файл------------------------------------------------
    private void saveFile(String filePath) {

        // объекты для записи в файл
        FileWriter outputStream = null;
        BufferedWriter outputBuffer = null;
        PrintWriter outputPrinter = null;

        String team;
        int played;
        int scored;
        int conceded;
        int points;

        try { // работа с файлами - всегда в блоке try-catch

            // поток для записи в файл
            outputStream = new FileWriter(filePath, false);

            // буфер для записи
            outputBuffer = new BufferedWriter(outputStream);

            outputPrinter = new PrintWriter(outputBuffer);

            // цикл по командам
            for (int i = 0; i < teams.size(); i++) {

                // получить значения
                team = teams.get(i).getTeamName();
                played = teams.get(i).getGamesPlayed();
                scored = teams.get(i).getGoalsScored();
                conceded = teams.get(i).getGoalsConceded();
                points = teams.get(i).getPoints();

                // если это не первая строка - поставить пустую строку
                if (i > 0) {
                    outputPrinter.println();
                }

                // запись данных в файл
                outputPrinter.print(team + ";" + played + ";" + scored + ";" + conceded + ";" + points);
            }

            // закрыть принтер, буфер и поток
            outputPrinter.close();
            outputBuffer.close();
            outputStream.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Output file error", JOptionPane.WARNING_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }

    //--------------------------------------------добавление результата матча-----------------------------------------------
    private void processMatchResult() {

        // получить индексы выбранных команд
        int homeTeamIndex = jlstHomeTeam.getSelectedIndex();
        int awayTeamIndex = jlstAwayTeam.getSelectedIndex();

        // получить введенный результат матча
        String matchResult = jtxtMatchResult.getText().trim();

        // если не выбрана первая команда
        if (homeTeamIndex == -1) {
            JOptionPane.showMessageDialog(this, "Выберите первую команду", "Ошибкка", JOptionPane.WARNING_MESSAGE);

            // если не выбрана вторая команда
        } else if (awayTeamIndex == -1) {
            JOptionPane.showMessageDialog(this, "Выберите вторую команду", "Ошибкка", JOptionPane.WARNING_MESSAGE);

            // если выбраны одинаковые команды
        } else if (homeTeamIndex == awayTeamIndex) {
            JOptionPane.showMessageDialog(this, "Команды должны быть разными", "Ошибкка", JOptionPane.WARNING_MESSAGE);

            // если не введен результат
        } else if (matchResult.length() < 3) {
            JOptionPane.showMessageDialog(this, "\n\nВведите результаты матча в формате Н:А\nН - голы, забитые первой командой, А - второй", "Ошибка", JOptionPane.WARNING_MESSAGE);

            // если все нормально - считаем голы, очки и пр.
        } else {
            try {
                // получить голы
                int homeGoals = Integer.parseInt(matchResult.split(":")[0]);
                int awayGoals = Integer.parseInt(matchResult.split(":")[1]);

                // записать результат для первой команды
                teams.get(homeTeamIndex).processMatch(homeGoals, awayGoals);

                // записать результат для второй команды
                teams.get(awayTeamIndex).processMatch(awayGoals, homeGoals);

                // обновить таблицы
                populateInfoTable();
                populateEPLTable();

                // сообщение пользователю
                String homeTeam = teams.get(homeTeamIndex).getTeamName();
                String awayTeam = teams.get(awayTeamIndex).getTeamName();
                String message = String.format("Результат успешно добавлен:%n%20s %d%n%20s %d", homeTeam, homeGoals, awayTeam, awayGoals);
                JOptionPane.showMessageDialog(this, message, "Результат матча", JOptionPane.INFORMATION_MESSAGE);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, e.getMessage() + "\n\nВведите результаты матча в формате Н:А\nН - голы, забитые первой командой, А - второй", "Ошибка", JOptionPane.WARNING_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e.getMessage() + "\n\nВведите результаты матча в формате Н:А\nН - голы, забитые первой командой, А - второй", "Ошибка", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
}