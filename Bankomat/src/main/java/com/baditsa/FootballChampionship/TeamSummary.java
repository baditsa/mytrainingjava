package com.baditsa.FootballChampionship;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 30.06.15
 * Time: 13:26
 * To change this template use File | Settings | File Templates.
 */
public class TeamSummary implements Comparable<TeamSummary> {

    // поля
    private String team;
    private int played;
    private int scored;
    private int conceded;
    private int points;

    // конструктор
    public TeamSummary(String teamName, int gamesPlayed, int goalsScored, int goalsConceded, int points) {
        team = teamName;
        played = gamesPlayed;
        scored = goalsScored;
        conceded = goalsConceded;
        this.points = points;
    }

    // гетеры
    public String getTeamName() {
        return team;
    }

    public int getGamesPlayed() {
        return played;
    }

    public int getGoalsScored() {
        return scored;
    }

    public int getGoalsConceded() {
        return conceded;
    }

    public int getPoints() {
        return points;
    }

    // сервис методы
    // подсчет разницы голов
    public int getGoalsDifference() {
        return scored - conceded;
    }

    // обработка результата матча
    public void processMatch(int goalsScored, int goalsConceded) {
        scored = scored + goalsScored;
        conceded = conceded + goalsConceded;

        // посчитать очки
        if (goalsScored > goalsConceded) {
            points = points + 3; // за победу 3 очка
        } else if (goalsScored == goalsConceded) {
            points = points + 1; // за ничью 1 очко
        }
    }

    // метод для сравнения объектов
    @Override
    public int compareTo(TeamSummary other) { // сравнение по очкам и голам

        // получение разницы
        int pointsDifference = this.getPoints() - other.getPoints();
        int goalsDifference = this.getGoalsDifference() - other.getGoalsDifference();

        // сначала сравним очки
        if (pointsDifference > 0) {
            return 1; // текущая команда выше
        } else if (pointsDifference < 0) {
            return -1; // текущая команда ниже
        } else {
            // если очки одинаковые - сравниваем по голам
            if (goalsDifference > 0) {
                return 1; // текущая команда выше
            } else if (goalsDifference < 0) {
                return -1; // текущая команда ниже
            } else {
                // если все показатели равны - две команды одинаковые
                return 0;
            }
        }
    }
}
