package com.baditsa.Clock;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 17.03.15
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
class Clock {
    String time;

    String getTime() {
        return time;
    }

    void setTime(String t) {
        time = t;
    }
}

class ClockTestDrive {
    public static void main(String[] args) {
        Clock c = new Clock();
        c.setTime("1245");
        String tod = c.getTime();
        System.out.println("время: " + tod);
    }
}