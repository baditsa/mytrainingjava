package com.baditsa.Chapter11;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 31.03.15
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */

import javax.sound.midi.*;

public class MiniMiniMusicApp {
    public static void main(String[] args) {
        MiniMiniMusicApp mini = new MiniMiniMusicApp();
        mini.play();
    }

    public void play() {

        try {
            Sequencer player = MidiSystem.getSequencer(); // Получаем синтезатор...
            player.open(); // ...и открываем его, чтобы начать использовать

            Sequence seq = new Sequence(Sequence.PPQ, 4); // Создаем новую последовательность

            Track track = seq.createTrack(); // Запрашиваем трек у последовательности

            ShortMessage a = new ShortMessage();
            a.setMessage(144, 1, 44, 100); // 144 - начало проигрывания ноты
            MidiEvent noteOn = new MidiEvent(a, 1);
            track.add(noteOn);                               // Помещаем
            // в трек
            ShortMessage b = new ShortMessage();             // MIDI-события
            b.setMessage(128, 1, 44, 100);
            MidiEvent noteOff = new MidiEvent(b, 16);
            track.add(noteOff);

            player.setSequence(seq); // Передаем последовательность синтезатору

            player.start(); // Запускаем синтезатор

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
