package com.baditsa.Dog;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 16.03.15
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */
class DogArray {
    String name;

    public static void main(String[] args) {

        // Создаем объект Dog и получаем к нему доступ
        Dog dog1 = new Dog();
        dog1.bark();
        dog1.name = "Барт";

        // Создаем массив типа Dog
        Dog[] myDogs = new Dog[3];

        // Помещаем в массив несколько элементов
        myDogs[0] = new Dog();
        myDogs[1] = new Dog();
        myDogs[2] = dog1;

        // Получаем доступ к объектам Dog с помощью ссылок из массива
        myDogs[0].name = "Фрэд";
        myDogs[1].name = "Джордж";

        System.out.print("Имя последней собаки - ");
        System.out.println(myDogs[2].name);

        // Переберем все элементы массива и вызовем для каждого метод bark ()
        int x = 0;
        while (x < myDogs.length) { // Массивы содержат переменную length, которая предоставляет количество хранимых элементов
            myDogs[x].bark();
            x = x + 1;
        }
    }

    public void bark() {
        System.out.println(name + "сказал Гав!");
    }

    public void eat() {
    }

    public void chaseCat() {
    }
}
