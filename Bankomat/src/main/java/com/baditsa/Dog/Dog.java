package com.baditsa.Dog;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 13.03.15
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */
// Создаем класс
class Dog {
    int size; // Переменная экземпляра
    String breed; // Переменная экземпляра
    String name; // Переменная экземпляра

    void bark() { // Метод
        System.out.println("Гав! Гав!");
    }
}

// Создаем проверочный класс (TestDrive)
class DogTestDrive {
    public static void main(String[] args) {

        // Создаем внутри тестового класса объект и получаем доступ к его переменным экземпляра и методам
        Dog d = new Dog(); // Создаем объект класса Dog
        d.size = 40; // Используем оператор доступа. Устанавливаем значение поля size
        d.bark(); // Вызываем метод bark ()
    }
}
