/*
package com.baditsa.PizzaStore;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 07.07.15
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 *//*

public abstract class PizzaStore {
//    SimplePizzaFactory factory;

//    public PizzaStore(SimplePizzaFactory factory) {
//        this.factory = factory;
//    }

    public Pizza orderPizza(String type) {
        Pizza pizza;

        pizza = createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    protected abstract Pizza createPizza(String type);
}
*/
