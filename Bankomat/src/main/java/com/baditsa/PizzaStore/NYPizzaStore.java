/*
package com.baditsa.PizzaStore;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 07.07.15
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 *//*

public class NYPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String item) {
        Pizza pizza = null;
        PizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

        if (item.equals("cheese")) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("New York Style Cheese Pizza");
            //return new NYStyleCheesePizza();

        } else if (item.equals("pepperoni")) {
            pizza = new PepperoniPizza(ingredientFactory);
            pizza.setName("New York Style Pepperoni Pizza");
            //return new NYStylePepperoniPizza();

        } else if (item.equals("clam")) {
            pizza = new ClamPizza(ingredientFactory);
            pizza.setName("New York Style Clam Pizza");
            //return new NYStyleClamPizza();

        } else if (item.equals("veggie")) {
            pizza = new VeggiePizza(ingredientFactory);
            pizza.setName("New York Style Veggie Pizza");
            //return new NYStyleVeggiePizza();
        }
        return pizza;
    }
}
*/
