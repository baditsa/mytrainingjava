package com.baditsa.SeaFight;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 18.03.15
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */
public class SimpleDotComTestDriveFinalVersion {

    int[] locationCells;
    int numOfHits = 0;

    public void setLocationCells(int[] locs) {
        locationCells = locs;
    }

    public String checkYourself(String stringGuess) {

        int guess = Integer.parseInt(stringGuess); // Преобразуем тип String в int

        String result = "Мимо"; // Создаем переменную для хранения результата, который будем возвращать. Присваиваем по умолчанию строковое значение "Мимо"

        for (int cell : locationCells) { // Повторяем это с каждым элементом массива

            if (guess == cell) {             //
                result = "Попал";            // Сравниваем ход пользователя с этим элементом массива
                numOfHits++;                 //

                break; // Выходим из цикла
            }
        } // Выходим из цикла
        if (numOfHits == locationCells.length) { // Выходим из цикла, но проверяем, "потоплены" ли мы (три попадания), и изменяем результат на "Потопил"
            result = "Потопил";
        }
        System.out.println(result); // Выводим пользователю результат ("Мимо", если он не был изменен на "Попал" или "Потопил")
        return result; // Возвращаем результат в вызывающий метод
    }
}

class SimpleDotComGame {
    public static void main(String[] args) {

        int numOfGuesses = 0; // Объявляем (создаем) переменную numOfGuesses типа int для хранения количества ходов пользователя: присваиваем ей 0

        GameHelper helper = new GameHelper(); // Это специальный класс, который содержит метод для приема пользовательского ввода

        SimpleDotCom theDotCom = new SimpleDotCom(); // Создаем новый экземпляр класс SimpleDotCom (создаем объект "сайт")

        int randomNum = (int) (Math.random() * 5); // Вычисляем (генерируем) случайное число от 0 до 4 для местоположения начальной (первой) ячейки...

        int[] locations = {randomNum, randomNum + 1, randomNum + 2}; // ...и используем его для формирования массива ячеек. Создаем целочисленный массив с местоположением трех ячеек и...

        theDotCom.setLocationCells(locations); // ...и вызываем метод setLocationCells() из экземпляра (передаем "сайту" местоположение его ячеек (массив)

        boolean isAlive = true; // Объявляем (создаем) булеву переменную isAlive, чтобы проверять в цикле,...

        while (isAlive == true) { // ...не закончилась ли игра (пока "сайт" не потоплен)

            String guess = helper.getUserInput("Введите число"); // Получаем строку, вводимую пользователем (пользовательский ввод)...

            String result = theDotCom.checkYourself(guess); // ...проверяем его. Вызываем метод checkYourself() из SimpleDotCom. Просим "сайт" проверить полученные данные; сохраняем возвращенный результат в переменную типа String

            numOfGuesses++; // Инкрементируем переменную numOfGuesses (инкрементируем количество попыток)

            if (result.equals("Потопил")) { // Если результат равен "Потопил"...

                isAlive = false; // ...присваиваем переменной isAlive значение false

                System.out.println("Вам потребовалось" + numOfGuesses + "попыток(и)"); // Выводим на экран количество попыток
            }

        }
    }

    private static class SimpleDotCom {
        private int[] locationCells;

        public void setLocationCells(int[] locationCells) {
            this.locationCells = locationCells;
        }

        public String checkYourself(String guess) {
            return null;  //To change body of created methods use File | Settings | File Templates.
        }
    }
}

class GameHelper {
    public String getUserInput(String prompt) {
        String inputLine = null;
        System.out.print(prompt + " ");
        try {
            BufferedReader is = new BufferedReader(
                    new InputStreamReader(System.in));
            inputLine = is.readLine();
            if (inputLine.length() == 0) return null;
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }
        return inputLine;
    }
}