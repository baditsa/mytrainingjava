package com.baditsa.DatingAgency;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 29.07.15
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
public class OwnerInvocationHandler implements InvocationHandler { // Все обработчики реализуют интерфейс InvocationHandler
    PersonBean person;

    public OwnerInvocationHandler(PersonBean person) { // Передаем реальный объект в конструкторе и сохраняем ссылку на него
        this.person = person;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws IllegalAccessException { // При каждом вызове метода заместителя вызывается метод invoke().
        try {
            if (method.getName().startsWith("get")) { // Если вызван get-метод - вызов передается реальному объекту
                return method.invoke(person, args);
            } else if (method.getName().equals("setHotOrNotRating")) { // Вызов метода setHotOrNotRating() блокируется выдачей исключения IllegalAccessException().
                throw new IllegalAccessException();
            } else if (method.getName().startsWith("set")) { // Вызов любых других set-методов владельцу разрешен, передаем реальному объекту.
                return method.invoke(person, args);
            }
        } catch (InvocationTargetException e) { // Выполняется при выдаче исключения реальным объектом.
            e.printStackTrace();
        }
        return null; // При вызове любого другого метода мы просто возвращаем null.
    }
}