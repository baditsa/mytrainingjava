package com.baditsa.DatingAgency;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 29.07.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
public interface PersonBean {

    // Получение ингформации о кандидате: имя, пол, увлечения и оценка (1-10).
    String getName();

    String getGender();

    String getInterests();

    int getHotOrNotRating();

    // Методы записи тех же данных
    void setName(String name);

    void setGender(String gender);

    void setInterests(String interests);

    void setHotOrNotRating(int rating); // Этот метод получает целое число и включает его в накапливаемую среднюю оценку кандидата.
}
