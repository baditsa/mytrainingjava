package com.baditsa.DatingAgency;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 29.07.15
 * Time: 14:17
 * To change this template use File | Settings | File Templates.
 */
public class PersonBeanImpl implements PersonBean {

    // Переменные экземпляров
    String name;
    String gender;
    String interests;
    int rating;
    int ratingCount = 0;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public String getInterests() {
        return interests;
    }

    @Override
    public int getHotOrNotRating() {    // Этот метод вычисляет среднюю оценку делением суммы на количество оценок ratingCount.
        if (ratingCount == 0) return 0;
        return (rating / ratingCount);
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public void setInterests(String interests) {
        this.interests = interests;
    }

    @Override
    public void setHotOrNotRating(int rating) { // Этот метод увеличивает значение ratingCount и прибавляет оценку к накапливаемой сумме
        this.rating += rating;
        ratingCount++;
    }
}