/*
package com.baditsa.DatingAgency;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 29.07.15
 * Time: 16:30
 * To change this template use File | Settings | File Templates.
 *//*

public class MatchMakingTestDrive {

    // Переменные экземпляров

    public static void main(String[] args) {
        MatchMakingTestDrive test = new MatchMakingTestDrive();
        test.drive();
    }

    public MatchMakingTestDrive() { // Конструктор инициализирует базу данных кандидатов службы знакомств
        initializeDatabase();
    }

    private void drive() {
        PersonBean joe = getPersonFromDatabase("Joe Javabean"); // Чтение записи из БД
        PersonBean ownerProxy = getOwnerProxy(joe); // и зоздание заместителя для владельца
        System.out.println("Name is " + ownerProxy.getName()); // Вызываем get-метод
        ownerProxy.setInterests("bowling, Go"); // затем set-метод
        System.out.println("Interests set from owner proxy");
        try {
            ownerProxy.setHotOrNotRating(10); // а затем пытаемся изменить оценку
        } catch (Exception e) {
            System.out.println("Can't set rating from owner proxy");
        }
        System.out.println("Rating is " + ownerProxy.getHotOrNotRating());

        PersonBean nonOwnerProxy = getNonOwnerProxy(joe); // Теперь создаем заместителя для другого пользователя
        System.out.println("Name is " + nonOwnerProxy.getName()); // Вызываем get-метод
        try {
            nonOwnerProxy.setInterests("bowling, Go"); // Вызываем set-метод
        } catch (Exception e) {
            System.out.println("Can't set interests from non owner proxy");
        }
        nonOwnerProxy.setHotOrNotRating(3); // Пытаемся задать оценку
        System.out.println("Rating set from non owner proxy");
        System.out.println("Rating is " + nonOwnerProxy.getHotOrNotRating());
    }
}*/
