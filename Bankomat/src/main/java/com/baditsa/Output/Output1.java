package com.baditsa.Output;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 20.03.15
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
class Output1 {
    public static void main(String[] args) {
        Output1 o = new Output1();
        o.go();
    }

    void go() {
        int y = 7;
        for (int x = 1; x < 8; x++) {
            y++;
            if (x > 4) {
                System.out.print(++y + " ");
            }
            if (y > 14) {
                System.out.println(" x = " + x);
                break;
            }
        }
    }
}
