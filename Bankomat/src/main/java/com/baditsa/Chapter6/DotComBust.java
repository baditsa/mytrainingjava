//package com.baditsa.Chapter6;
//
//import java.util.ArrayList;
//
///**
// * Created with IntelliJ IDEA.
// * User: Алексей
// * Date: 20.03.15
// * Time: 15:49
// * To change this template use File | Settings | File Templates.
// */
//public class DotComBust {
//
//    private GameHelper helper = new GameHelper ();
//    private ArrayList<DotCom> dotComList = new ArrayList<DotCom>();
//    private int numOfGueses = 0;
//
//    private void setUpGame() {
//        DotCom one = new DotCom();
//        one.setName("Pets.com");
//
//        DotCom two = new DotCom();
//        two.setName("eToys.com");
//
//        DotCom three = new DotCom();
//        three.setName("Go2.com");
//
//        dotComsList.add(one);
//        dotComsList.add(two);
//        dotComsList.add(three);
//
//        System.out.println("Ваша цель - потопить три сайта");
//        System.out.println("Pets.com, eToys.com, Go2.com");
//        System.out.println("Попытайтесь потопить их за минимальное количество ходов");
//
//        for (DotCom dotComToSet : dotComList) {
//
//            ArrayList<String> newLocation = helper.placeDotCom(3);
//
//            dotComToSet.setLocationCells(newLocation);
//        }
//    }
//
//    private void startPlaying() {
//
//        while(!dotComList.isEmpty()) {
//
//            String userGuess = helper.getUserInput("Сделайте ход");
//
//            checkUserGuess(userGuess);
//        }
//        finishGame();
//    }
//
//    private void checkUserGuess (String userGuess) {
//
//        numOfGueses++;
//
//        String result = "Мимо";
//
//        for (DotCom dotComToTest : dotComsList) {
//
//            result = dotComToTest.checkYourself(userGuess);
//
//            if (result.equals("Попал")) {
//                break;
//            }
//            if (result.equals("Потопил")) {
//                dotComList.remove(dotComToTest);
//                break;
//            }
//        }
//        System.out.println(result);
//    }
//
//    private void finishGame() {
//        System.out.println("Все сайты ушли ко дну! Ваши акции теперь ничего не стоят");
//        if (numOfGueses <= 18) {
//            System.out.println("Это заняло у вас всего" + numOfGueses + "попыток.");
//            System.out.println("Вы успели выбраться до того, как ваши вложения утонули.");
//        } else {
//            System.out.println("Это заняло у вас довольно много времени. "+ numOfGueses + "попыток.");
//            System.out.println("Рабы водят хороводы вокруг ваших вложений.");
//        }
//    }
//    public static void main (String[] args) {
//        DotComBust game = new DotComBust();
//        game.setUpGame();
//        game.startPlaying();
//    }
//}
//
