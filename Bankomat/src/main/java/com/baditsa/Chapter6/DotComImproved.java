package com.baditsa.Chapter6;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 20.03.15
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
public class DotComImproved {

    private ArrayList<String> locationCells; // Строковый массив...
    private String name;

    public void setLocationCells(ArrayList<String> loc) { // ...для хранения объектов String // Сеттер, который обновляет местоположение сайта
        locationCells = loc;
    }

    public void setName(String n) { // Простой сеттер
        name = n;
    }

    public String checkYourself(String userInput) {

        String result = "Мимо";

        int index = locationCells.indexOf(userInput); // Проверяем, содержится ли загаданная пользователем ячейка внутри ArrayList, запрашивая ее индекс. Если ее нет в списке, то indexOf () возвращает -1

        if (index >= 0) { // Если индекс больше или равен нулю, то загаданная пользователем ячейка определенно находится в списке,...
            locationCells.remove(index); // ...поэтому удаляем ее

            if (locationCells.isEmpty()) { // Если список пустой, значит, это было роковое попадание
                result = "Потопил";
                System.out.println("Ой! Вы потопили" + name + " : ( ");
            } else {
                result = "Попал";
            }
        }
        return result;
    }
}
