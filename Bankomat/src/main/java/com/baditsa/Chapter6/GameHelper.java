package com.baditsa.Chapter6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 23.03.15
 * Time: 14:22
 * To change this template use File | Settings | File Templates.
 */
public class GameHelper {

    private static final String alphabet = "abcdefg";
    private int gridLenght = 7;
    private int gridSize = 49;
    private int[] grid = new int[gridSize];
    private int comCount = 0;

    public String getUserInput(String prompt) {
        String inputLine = null;
        System.out.print(prompt + " ");
        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
            inputLine = is.readLine();
            if (inputLine.length() == 0) return null;
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }
        return inputLine.toLowerCase();
    }

    public ArrayList<String> placeDotCom(int comSize) {
        ArrayList<String> alphaCells = new ArrayList<String>();
        String[] alphacoords = new String[comSize]; // Хранит координаты типа f6
        String temp = null; // Временная строка для конкатенации
        int[] coords = new int[comSize]; // Координаты текущего сайта
        int attempts = 0; // Счетчик текущих попыток
        boolean succes = false; // Нашли подходящее местоположение?
        int location = 0; // Текущее начальное местоположение

        comCount++; // Энный сайт для размещения
        int incr = 1; // Устанавливаем горизонтальный инкремент
        if ((comCount % 2) == 1) { // Если нечетный (размещаем вертикально)
            incr = gridLenght; // Устанавливаем вертикальный инкремент
        }
        while (!succes & attempts++ < 200) { // Главный поисковый цикл (32)
            location = (int) (Math.random() * gridSize); // Получаем случайную стартовую точку
            int x = 0; // Энная позиция в сайте, который нужно расместить
            succes = true; // Предполагаем успешный исход
            while (succes && x < comSize) { // Ищем соседнюю неиспользованную ячейку
                if (grid[location] == 0) { // Если еще не используется
                    coords[x++] = location; // Сохраняем местоположение
                    location += incr; // Пробуем следующую соседнюю ячейку
                    if (location >= gridSize) { // Вышли за рамки - низ
                        succes = false; // Неудача
                    }
                    if (x > 0 && (location % gridLenght == 0)) { // Вышли за рамки - правый край
                        succes = false; // Неудача
                    }
                } else { // Нашли уже использующееся местоположение
                    succes = false; // Неудача
                }
            }
        }
        int x = 0;
        int row = 0;
        int column = 0;

        while (x < comSize) {
            grid[coords[x]] = 1;
            row = (int) (coords[x] / gridLenght);
            column = coords[x] % gridLenght;
            temp = String.valueOf(alphabet.charAt(column));

            alphaCells.add(temp.concat(Integer.toString(row)));
            x++;
        }
        return alphaCells;
    }
}
