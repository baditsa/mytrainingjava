package com.baditsa.Menu;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 20.07.15
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
// class MenuComponent предоставляет реализации по умолчанию для всех методов. Т.к. одни методы имеют смысл только для
// MenuItem, а другие - для Menu, реализация по умолчанию инициирует UnsupportedOperationException (исключение времени выполнения)
// Если объект MenuItem или Menu не поддерживает операцию, ему не нужно ничего делать - он просто наследует реализацию по умолчанию
public abstract class MenuComponent {

    // группа "комбинационных" методов - т.е. методов для добавления, удаления и получения MenuComponent
    public void add(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    public void remove(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    public MenuComponent getChild(int i) {
        throw new UnsupportedOperationException();
    }

    // группа "методов операций", используемых в MenuItem
    public String getName() {
        throw new UnsupportedOperationException();
    }

    public String getDescription() {
        throw new UnsupportedOperationException();
    }

    public double getPrice() {
        throw new UnsupportedOperationException();
    }

    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }

    // метод print() реализуется как в Menu, так и в MenuItem, но мы предоставляем реализацию по умолчанию
    public void print() {
        throw new UnsupportedOperationException();
    }
}