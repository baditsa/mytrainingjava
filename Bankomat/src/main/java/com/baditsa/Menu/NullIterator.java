package com.baditsa.Menu;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 21.07.15
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
public class NullIterator implements Iterator {

    @Override
    public Object next() { // При вызове next() возвращается null
        return null;
    }

    @Override
    public boolean hasNext() { // При вызове hasNext() всегда возвращается false
        return false;
    }

    public void remove() { // Не поддерживает удаление
        throw new UnsupportedOperationException();
    }
}