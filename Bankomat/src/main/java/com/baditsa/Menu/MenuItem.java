package com.baditsa.Menu;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 17.07.15
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */
// class MenuItem реализует поведение элементов комбинации
public class MenuItem {
    String name;
    String description;
    boolean vegetarian;
    double price;

    public MenuItem(String name, String description, boolean vegetarian, double price) {
        this.name = name;
        this.description = description;
        this.vegetarian = vegetarian;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public double getPrice() {
        return price;
    }

    // переопределяем метод print() класса MenuComponent. Для MenuItem этот метод выводит полное описание элемента меню:
    // название, описание, цену и признак вегетарианского блюда
    public void print() {
        System.out.print(" " + getName());
        if (isVegetarian()) {
            System.out.print("(v)");
        }
        System.out.println(", " + getPrice());
        System.out.println("   -- " + getDescription());
    }

    public Iterator createIterator() {
        return new NullIterator();
    }
}