/*
package com.baditsa.Menu;

import java.util.ArrayList;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 21.07.15
 * Time: 14:09
 * To change this template use File | Settings | File Templates.
 *//*

public class Menu extends MenuComponent {
    Iterator iterator = null;
    ArrayList menuComponents = new ArrayList();   // Menu может иметь любое количество потомков типа MenuComponent.
    String name;                                  // Для их хранения будет использоваться внутренняя коллекция ArrayList
    String description;

    // Конструктор
    public Menu(String name, String description) {
        this.name = name;
        this.description = description;
    }
//----------------------------------------------------------------------------------------------------------------------
    public void add(MenuComponent menuComponent) {
        menuComponents.add(menuComponent);
    }

    public void remove(MenuComponent menuComponent) {
        menuComponents.remove(menuComponent);
    }

    public MenuComponent getChild(int i) {
        return (MenuComponent)menuComponents.get(i);
    }
//----------------------------------------------------------------------------------------------------------------------
    public String getName() {                    // Методы getPrice() и isVegetarian() не переопределяются,
        return name;                             // т.к. не имеют смысла для Menu. При попытке вызвать их для Menu
    }                                            // будет выдано исключение UnsupportedOperationException.

    public String getDescription() {
        return description;
    }
//----------------------------------------------------------------------------------------------------------------------
    public void print() {
        System.out.print("\n" + getName());
        System.out.println(", " + getDescription());
        System.out.println("---------------------");
    }
//----------------------------------------------------------------------------------------------------------------------
    public Iterator createIterator() {
        if (iterator == null) {
            iterator = new CompositeIterator(menuComponents.iterator());
        }
        return iterator;
    }
}*/
