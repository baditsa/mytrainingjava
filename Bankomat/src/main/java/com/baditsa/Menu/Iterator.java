package com.baditsa.Menu;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 17.07.15
 * Time: 18:08
 * To change this template use File | Settings | File Templates.
 */
public interface Iterator {
    boolean hasNext(); // возвращает флаг, который указывает, остались ли в коллекции элементы для перебора

    Object next(); // возвращает следующий элемент
}
