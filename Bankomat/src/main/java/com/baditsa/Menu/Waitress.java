/*
package com.baditsa.Menu;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 17.07.15
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 *//*

public class Waitress {
    MenuComponent allMenus;

    // передаем компонент меню верхнего уровня - который содержит остальные меню (allMenus)
    public Waitress(MenuComponent allMenus){
        this.allMenus = allMenus;
    }

    // чтобы вывести всю иерархию меню (все меню и все их элементы) достаточно вызвать метод print() для меню верхнего уровня
    public void printMenu() {
        allMenus.print();
    }

    public void printVegetarianMenu() { // этот метод получает комбинацию allMenu и создает для нее итератор (наш класс CompositeIterator)
        Iterator iterator = allMenus.createIterator();
        System.out.println("\nVEGETARIAN MENU\n----");
        while (iterator.hasNext()) { // перебор всех элементов комбинации
            MenuComponent menuComponent = (MenuComponent)iterator.next();
            try {
                if (menuComponent.isVegetarian()) { // для каждого элемента вызывается метод isVegetarian(), и если он возвращает true - для элемента вызывается метод print()
                    menuComponent.print(); // метод print() вызывается только для MenuItem, и никогда для комбинаций
                }
            } catch (UnsupportedOperationException e) {} // реализация isVegetarian() для Menu всегда генерирует исключение.
        }                                                // Если это произойдет, мы перехватываем исключение и продолжаем перебор
    }
}*/
