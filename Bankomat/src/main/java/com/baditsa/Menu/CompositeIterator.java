/*
package com.baditsa.Menu;

import java.util.Stack;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 21.07.15
 * Time: 15:07
 * To change this template use File | Settings | File Templates.
 *//*

public class CompositeIterator implements Iterator {
    Stack stack = new Stack();

    public CompositeIterator(Iterator iterator) { // Здесь передается итератор комбинации верхнего уровня.
        stack.push(iterator);                     // Мы сохраняем его в стеке
    }

    @Override
    public Object next() {
        if (hasNext()) {  // Когда клиент запрашивает следующий элемент, мы сначала проверяем его существование вызовом hasNext()
            Iterator iterator = (Iterator) stack.peek();
            MenuComponent component = (MenuComponent) iterator.next();
            if (component instanceof Menu) { // Если следующий элемент существует, мы извлекаем текущий итератор из стека и получаем следующий элемент
                stack.push(component.createIterator()); // Если компонент относится к классу Menu - обнаружена очередная комбинация, которую необходимо включить в перебор.
            }                                           // Соответственно мы заносим его в стек.
            return component;                           // В любом случае метод возвращает компонент.
        } else {
            return null;
        }
    }

    @Override
    public boolean hasNext() {
        if (stack.empty()) { // Чтобы проверить наличие следующего компонента, мы проверяем, пуст ли стек.
            return false;
        } else {
            Iterator iterator = (Iterator) stack.peek(); // Если стек не пуст, читаем из стека верхний итератор
            if (!iterator.hasNext()) {                   // и проверяем, есть ли в стеке следующий элемент.
                stack.pop();                             // Если нет - метод извлекает компонент из стека
                return hasNext();                        // и рекурсивно вызывает hasNext().
            } else {
                return true;                             // А если есть - метод возвращает true
            }
        }
    }

    public void remove() {
        throw new UnsupportedOperationException(); // Удаление не поддерживается - только перебор.
    }
}*/
