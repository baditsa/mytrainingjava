package com.baditsa.Menu;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 17.07.15
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class DinerMenuIterator implements Iterator {
    MenuItem[] list;
    int position = 0; // в переменной position хранится текущая позиция перебора в массиве

    public DinerMenuIterator(MenuItem[] list) { // конструктор получает массив объектов, для перебора которых создается итератор
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        if (position >= list.length || list[position] == null) { // т.к. для меню бистро выделен массив максимального размера, нужно проверить не только достижение границы массива, но и равенство следующего элемента null (признак последнего элемента)
            return false;
        } else {
            return true; // метод hasNext() возврщает true, если в массиве еще остались элементы для перебора
        }
    }

    @Override
    public Object next() { // метод next() возвращает следующий элемент массива и увеличивает текущую позицию
        MenuItem menuItem = list[position];
        position = position + 1;
        return menuItem;
    }

    public void remove() {
        if (position <= 0) {
            throw new IllegalStateException("You can't remove an item util you've done at least one next()");
        }
        if (list[position - 1] != null) {
            for (int i = position - 1; i < (list.length - 1); i++) {
                list[i] = list[i + 1];
            }
            list[list.length - 1] = null;
        }
    }
}