package com.baditsa.Starbuzz;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 17:12
 * To change this template use File | Settings | File Templates.
 */
public class Decaf extends Beverage {

    public Decaf() {
        description = "Decaf";
    }

    @Override
    public double cost() {
        return 1.05;
    }
}