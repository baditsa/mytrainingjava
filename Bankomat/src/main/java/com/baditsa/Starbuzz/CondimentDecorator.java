package com.baditsa.Starbuzz;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
public abstract class CondimentDecorator extends Beverage {
    public abstract String getDescription();
}