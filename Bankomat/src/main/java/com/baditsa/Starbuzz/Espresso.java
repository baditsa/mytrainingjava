package com.baditsa.Starbuzz;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */
public class Espresso extends Beverage {

    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 1.99;
    }
}
