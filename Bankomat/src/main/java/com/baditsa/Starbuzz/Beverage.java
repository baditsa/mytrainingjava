package com.baditsa.Starbuzz;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 16:54
 * To change this template use File | Settings | File Templates.
 */
public abstract class Beverage {
//    public static final double TALL = .10;
//    public static final double GRANDE = .15;
//    public static final double VENTI = .20;

    String description = "Unknown Beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();

//    public abstract int getSize();
}