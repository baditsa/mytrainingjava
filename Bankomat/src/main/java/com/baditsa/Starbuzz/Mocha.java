package com.baditsa.Starbuzz;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.07.15
 * Time: 17:16
 * To change this template use File | Settings | File Templates.
 */
public class Mocha extends CondimentDecorator {
    Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Mocha";
    }

    @Override
    public double cost() {
        return .20 + beverage.cost();
    }
}