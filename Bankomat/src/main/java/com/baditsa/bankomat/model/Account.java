package com.baditsa.bankomat.model;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 23.06.15
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
// класс аккаунт (счет) хранит деньги
public class Account {

    private double balance;

    // конструктор
    public Account(double bal) {
        balance = bal;
    }

    // получить баланс
    public double getBalance() {
        return balance;
    }

    // добавить (положить) деньги
    public void deposit(double money) {
        balance = balance + money;
    }

    // снять деньги
    public void withdraw(int money) {
        balance = balance - money;
    }
}
