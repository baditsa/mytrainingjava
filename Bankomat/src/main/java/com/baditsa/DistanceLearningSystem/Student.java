package com.baditsa.DistanceLearningSystem;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 29.04.15
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public class Student implements Serializable {

    private int id; // Регистрационный номер ученика
    private Course course; // Изучаемый курс
    private Topic topic; // Изучаемая тема курса
    private Task task; // Вопросы, предложенные ученику
    private int mark; // Текущая оценка

    public Student() {
    }

    public Student(int id, Course course, Topic topic, Task task, int mark) {
        this.id = id;
        this.course = course;
        this.topic = topic;
        this.task = task;
        this.mark = mark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    private Topic getTopic() {
        return topic;
    }

    private void setTopic(Topic topic) {
        this.topic = topic;
    }

    private Task getTask() {
        return task;
    }

    private void setTask(Task task) {
        this.task = task;
    }

    private int getMark() {
        return mark;
    }

    private void setMark(int mark) {
        this.mark = mark;
    }

    public void addMark(int newMark) {
        mark += newMark;
    }

    public boolean isNewcomer() {
        return id == 0;
    }

    public boolean isStudent() {
        return id > 0;
    }

    public String toString() {
        return "Student: " + id + " " + course + " " + task + " " + mark;
    }
}