package com.baditsa.DistanceLearningSystem;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.StringTokenizer;
import java.util.Vector;

import static java.awt.GridBagConstraints.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 12.05.15
 * Time: 18:14
 * To change this template use File | Settings | File Templates.
 */
public class RegPrep extends JFrame {
    public RegPrep() throws HeadlessException {
        super();
        init();
    }

    private int id;
    private JTextField tf1, tf2, tf3, tf4;
    private JTextArea ta;
    private JButton b1, b2;
    private String fullname = "", name = "", secname = "", surname = "", addr = "";

    private String driver = "oracle.jdbc.OracleDriver",
            url = "jdbc:oracle:thin:@ann:1521:accotest",
            user = "system",
            password = "manager";

    private Vector<String> courses = new Vector<String>();

    private java.sql.Statement st;

    public void init() {
       /* driver = getParameter("driver");
        url = getParameter("url");
        user = getParameter("user");
        password = getParameter("password");*/

        JPanel p = new JPanel();

        getContentPane().add(p);

        p.setBackground(Color.white);
        p.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(3, 3, 3, 3);

        JLabel lReg1 = new JLabel("Для регистрации занесите сведения о себе");
        lReg1.setFont(new Font("Serif", Font.PLAIN, 18));

        JLabel lReg2 = new JLabel("в следующие поля:");
        lReg2.setFont(new Font("Serif", Font.PLAIN, 20));

        JLabel l1 = new JLabel("Фамилия:", JLabel.RIGHT);
        l1.setLabelFor(tf1);

        JLabel l2 = new JLabel("Имя:", JLabel.RIGHT);
        l2.setLabelFor(tf2);


        JLabel l3 = new JLabel("Отчество:", JLabel.RIGHT);
        l3.setLabelFor(tf3);


        JLabel l4 = new JLabel("E-mail:", JLabel.RIGHT);
        l4.setLabelFor(tf4);


        JLabel l5 = new JLabel("Замечания:", JLabel.LEFT);
        l5.setLabelFor(ta);


        tf1 = new JTextField(30);
        tf1.setMinimumSize(new Dimension(250, 25));
        tf1.setPreferredSize(new Dimension(250, 25));

        tf2 = new JTextField(30);
        tf2.setMinimumSize(new Dimension(250, 25));
        tf2.setPreferredSize(new Dimension(250, 25));

        tf3 = new JTextField(30);
        tf3.setMinimumSize(new Dimension(250, 25));
        tf3.setPreferredSize(new Dimension(250, 25));


        tf4 = new JTextField(30);
        tf4.setMinimumSize(new Dimension(250, 25));
        tf4.setPreferredSize(new Dimension(250, 25));

        ta = new JTextArea(1, 30);
        ta.setMinimumSize(new Dimension(250, 25));
        ta.setPreferredSize(new Dimension(250, 125));
        ta.setEditable(false);


        b1 = new JButton("Зарегистрировать");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                register();
            }
        });

        p.add(lReg1, new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));
        p.add(lReg2, new GridBagConstraints(0, RELATIVE, 2, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));
        p.add(l1, new GridBagConstraints(0, RELATIVE, 1, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));
        p.add(l2, new GridBagConstraints(0, RELATIVE, 1, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));
        p.add(l3, new GridBagConstraints(0, RELATIVE, 1, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));
        p.add(l4, new GridBagConstraints(0, RELATIVE, 1, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));
        p.add(l5, new GridBagConstraints(0, RELATIVE, 1, 1, 1.0, 1.0, LINE_END, HORIZONTAL, gbc.insets, 0, 0));

        p.add(tf1, new GridBagConstraints(1, RELATIVE, 1, 1, 1.0, 1.0, CENTER, HORIZONTAL, gbc.insets, 0, 0));
        p.add(tf2, new GridBagConstraints(1, RELATIVE, 1, 1, 1.0, 1.0, CENTER, HORIZONTAL, gbc.insets, 0, 0));
        p.add(tf3, new GridBagConstraints(1, RELATIVE, 1, 1, 1.0, 1.0, CENTER, HORIZONTAL, gbc.insets, 0, 0));
        p.add(tf4, new GridBagConstraints(1, RELATIVE, 1, 1, 1.0, 1.0, CENTER, HORIZONTAL, gbc.insets, 0, 0));
        p.add(ta, new GridBagConstraints(1, RELATIVE, 1, 1, 1.0, 1.0, CENTER, HORIZONTAL, gbc.insets, 0, 0));

        p.add(b1, new GridBagConstraints(0, RELATIVE, 2, 1, 1.0, 1.0, CENTER, NONE, gbc.insets, 0, 0));
        pack();
        try {
            Class.forName(driver);
        } catch (Exception e) {
            ta.append("From init exc: " + e + "\n");
        }
    }

    public void register() {
        surname = tf1.getText();
        name = tf2.getText();
        secname = tf3.getText();
        addr = tf4.getText();

        if (surname.length() * name.length() * secname.length() * addr.length() == 0) {
            JOptionPane.showMessageDialog(this, "Заполните все поля", "Замечание", JOptionPane.ERROR_MESSAGE);
            return;
        }

        fullname = surname.trim() + " " + name.trim() + " " + secname.trim();

        try {
            Connection con = DriverManager.getConnection(url, user, password);

            PreparedStatement pst = con.prepareStatement("INSERT INTO students (id, name, address) VALUES(reg_seq.NEXTVAL, ?, ?)");
            pst.setString(1, fullname);
            pst.setString(2, addr);
            int count = pst.executeUpdate();
            pst.close();

            st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name FROM students ORDER BY id DESC");

            rs.next();

            id = rs.getInt(1);
            fullname = rs.getString(2);

            rs.close();

            courseSelect();

            con.close();

        } catch (Exception e) {
            ta.append("Проверьте Ваши сведения\n" + e);
        }
    }

    public void courseSelect() throws SQLException {

        JPanel c1 = new JPanel();
        c1.setLayout(new GridBagLayout());
        c1.setBackground(Color.white);
        c1.setFont(new Font("Serif", Font.PLAIN, 16));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(3, 3, 3, 3);

        StringTokenizer sttok = new StringTokenizer(fullname);
        sttok.nextToken();
        name = sttok.nextToken();
        secname = sttok.nextToken();

        JLabel l6 = new JLabel("Добро пожаловать, " + name + " " + secname + "!");
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridx = 0;
        c1.add(l6, gbc);

        JLabel l66 = new JLabel("Вы зарегистрированы в СДО");
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridx = 0;
        c1.add(l66, gbc);

        JLabel l7 = new JLabel("Ваш регистрационный " + id);
        gbc.gridx = 0;
        c1.add(l7, gbc);

        JLabel l8 = new JLabel("Выберите учебный курс: ");
        gbc.gridx = 0;
        c1.add(l8, gbc);

        ResultSet rs = st.executeQuery("SELECT dining.course_name FROM dining.courses");

        while (rs.next()) courses.addElement(rs.getString(1));
        rs.close();

        JList list = new JList(courses);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                getCourse(e.getFirstIndex());
            }
        });

        JScrollPane sp = new JScrollPane(list);
        gbc.gridx = 0;
        c1.add(sp, gbc);

        setContentPane(c1);
    }

    private void getCourse(int firstIndex) {
        courses.get(firstIndex);

        try {
            Connection con = DriverManager.getConnection(url, user, password);

            PreparedStatement pst = con.prepareStatement("UPDATE dining.students SET dining.courses = ? Where dining.id = ? ");
            pst.setString(1, courses.get(firstIndex));
            pst.setInt(2, id);
            pst.executeUpdate();
            pst.close();

            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        new RegPrep().setVisible(true);
    }
}