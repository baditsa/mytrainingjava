//package com.baditsa.DistanceLearningSystem;
//
//import javax.servlet.http.*;
//import java.io.*;
//import java.sql.*;
//import java.util.StringTokenizer;
//
///**
// * Created with IntelliJ IDEA.
// * User: Алексей
// * Date: 18.05.15
// * Time: 13:56
// * To change this template use File | Settings | File Templates.
// */
//public class RegPrepServlet extends HttpServlet {
//
//    private String driver = "oracle.jdbc.OracleDriver",
//            url = "jdbc:oracle:thin:@ann:1521:accotest",
//            user = "system",
//            password = "manager";
//
//    public void init(){
//
//        try{
//            Class.forName(driver);
//        } catch (Exception e) {
//            System.err.println("From init(): " + e);
//        }
//    }
//
//    public void doGet(HttpServletRequest req, HttpServletResponse resp){
//        doPost(req, resp);
//    }
//
//    public void doPost(HttpServletRequest req, HttpServletResponse resp){
//
//        try{
//            req.setCharacterEncoding("Cp1251");
//
//            String surname = req.getParameter("surname");
//            String name = req.getParameter("name");
//            String secname = req.getParameter("secname");
//            String addr = req.getParameter("addr");
//
//            resp.setContentType("text/html; charset=windows-1251");
//
//            PrintWriter pw = resp.getWriter();
//
//            if (surname.length() * name.length() * secname.length() * addr.length() == 0) {
//                pw.println("<html><head>");
//                pw.println("<title>Продолжение регистрации</title>");
//                pw.println("</head><body><h2 align=center>" + "Дистанционная система обучения СДО</h2>");
//                pw.println("<h3>Замечание:</h3>");
//                pw.println("Заполните все поля.<br>");
//                pw.println("</body><html>");
//                pw.flush();
//                pw.close();
//                return;
//            }
//
//            String fullname = surname.trim() + " " + name.trim() + " " + secname.trim();
//
//            Connection con = DriverManager.getConnection(url, user, password);
//
//            PreparedStatement pst = con.prepareStatement
//                    ("INSERT INTO students (id, name, address) " +
//                     "VALUES(reg_seq.NEXTVAL, ?, ?)");
//            pst.setString(1, fullname);
//            pst.setString(2, addr);
//            int count = pst.executeUpdate();
//            pst.close();
//
//            Statement st = con.createStatement();
//
//            ResultSet rs = st.executeQuery("SELECT id, name FROM students ORDER BY id DESC");
//
//            rs.next();
//
//            int id = rs.getInt(1);
//            fullname = rs.getString(2);
//            rs.close();
//
//            StringTokenizer sttok = new StringTokenizer(fullname);
//            sttok.nextToken();
//            name = sttok.nextToken();
//            secname = sttok.nextToken();
//
//            pw.println("<html><head>");
//            pw.println("<title>Регистрация</title>");
//            pw.println("</head><body><h2 align=center>" + "Дистанционная система обучения СДО</h2>");
//            pw.println("Добро пожаловать, " + name + " " + secname + "!<br>");
//            pw.println("Вы зарегистрированы в СДО.<br>");
//            pw.println("Ваш регистрационный номер "  + id + "<br>");
//            pw.println("Выберите учебный курс:<br> ");
//
//            rs = st.executeQuery("SELECT dining.course_name FROM dining.courses");
//
//            pw.println("<form method=post action=" + "\"http://homexp:8000/InfoAppl/servlet/CoursesServlet\">");
//            pw.println("<select size=5 name=courses>");
//
//            while (rs.next()) pw.println("<option>" + rs.getString(1));
//            pw.println("</option></form></body></html>");
//            pw.flush();
//            pw.close();
//
//            rs.close();
//            con.close();
//        } catch (Exception e) {
//            System.err.println(e);
//        }
//    }
//}