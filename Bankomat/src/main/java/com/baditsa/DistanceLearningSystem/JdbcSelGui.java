//package com.baditsa.DistanceLearningSystem;
//
//import javax.swing.*;
//import javax.swing.table.AbstractTableModel;
//import java.awt.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.sql.*;
//import java.util.Vector;
//
///**
// * Created with IntelliJ IDEA.
// * User: Алексей
// * Date: 22.04.15
// * Time: 16:01
// * To change this template use File | Settings | File Templates.
// */
//public class JdbcSelGui extends JFrame {
//    Connection con;
//    JTextField addr = new JTextField("jdbc:oracle:thin:@ann:1521:accotest");
//    JTextField name = new JTextField("APP");
//    JPasswordField pass = new JPasswordField("APP");
//    JTextField tf = new JTextField("SELECT name, address FROM students");
//    JLabel sf = new JLabel("Строка состояния");
//    JButton b = new JButton("Выполнить");
//    JTable table;
//    DBTableModel model;
//
//    public JdbcSelGui() {
//        super("Выборка");
//        Container c = getContentPane();
//
//        JPanel p1 = new JPanel(false);
//        p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
//        c.add(p1, BorderLayout.NORTH);
//
//        JPanel p3 = new JPanel(false);
//        p3.setLayout(new GridLayout(0, 1));
//        p1.add(p3);
//
//        p3.add(new JLabel("Адрес ", JLabel.RIGHT));
//        p3.add(new JLabel("Имя ", JLabel.RIGHT));
//        p3.add(new JLabel("Пароль ", JLabel.RIGHT));
//        p3.add(new JLabel("Запрос ", JLabel.RIGHT));
//        p3.add(new JLabel());
//
//        JPanel p4 = new JPanel(false);
//        p4.setLayout(new GridLayout(0, 1));
//        p1.add(p4);
//        p4.add(addr);
//        p4.add(name);
//        p4.add(pass);
//        p4.add(tf);
//
//        String[] defCols = {"Имя", "Адрес"};
//        Object[] defData = {"", ""};
//
//        model = new DBTableModel(defCols, defData, 0);
//        table = new JTable(model);
//
//        table.setRowHeight(20);
//        table.setToolTipText("Содержимое выборки");
//
//        sf.setToolTipText("Строка состояния");
//        sf.setBackground(Color.lightGray);
//        sf.setPreferredSize(new Dimension(300, 29));
//
//        tf.setToolTipText(" Введите сюда запрос");
//        tf.setPreferredSize(new Dimension(100, 29));
//
//        b.setText("Выполнить");
//        b.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent evt) {
//                connect(addr.getText(), name.getText(), pass.getPassword());
//                selectRecords (tf.getText());
//            }
//        });
//
//        JScrollPane sp = new JScrollPane(table);
//
//        JPanel p = new JPanel();
//        p.add(b);
//        p4.add(p);
//
//        c.add(sp);
//        c.add(sf, BorderLayout.SOUTH);
//
//        setSize(400, 300);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setVisible(true);
//    }
//
//    public void connect(String addr, String name, char[] pass) {
//        try {
//            sf.setText("Идет соединение...");
//
//            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
//
//            String s = new String(pass);
//            con = DriverManager.getConnection(addr, name, s);
//            con.setAutoCommit(false);
//
//            sf.setText("Успешное соединение");
//
//        } catch (Exception e) {
//            sf.setText("Ошибка соединения... " + e);}
//    }
//
//    public void selectRecords(String query) {
//        try {
//            Statement st = con.createStatement();
//            ResultSet rs = st.executeQuery(query);
//
//            model.clearTable();
//            model.insertFromDB(rs);
//
//            rs.close();
//            st.close();
//        } catch (SQLException e) {sf.setText("Ошибка выборки... " + e);}
//    }
//
//    public static void main(String[] args) {
//        new JdbcSelGui();
//    }
//}
//
//class DBTableModel extends AbstractTableModel {
//
//    int cols;
//    Vector data;
//    String[] colNames;
//
//    public DBTableModel(String[] colNames, Object[] values, int rows) {
//
//        cols = colNames.length;
//
//        this.colNames = new String[cols];
//        for (int i = 0; i < cols; i++) this.colNames[i] = colNames[i];
//
//        data = new Vector();
//        for (int i=0; i < rows; i++) {
//            Vector v = new Vector();
//            for (int j = 0; j < cols; j++) v.addElement(values[j]);
//            data.addElement(v);
//        }
//    }
//
//    public Object getValueAt(int row, int col) {
//        Vector v= (Vector)data.elementAt(row);
//        return v.elementAt(col);
//    }
//
//    public int getRowCount(){
//        return data.size();
//    }
//
//    public int getColumnCount(){
//        return cols;
//    }
//
//    public void setValueAt(Object obj, int row, int col) {
//        Vector v = (Vector)data.elementAt(row);
//        v.setElementAt(obj, col);
//    }
//
//    public void insertRow(Vector row) {
//        data.addElement(row);
//        super.fireTableDataChanged();
//    }
//
//    public void deleteRow(int row) {
//        data.removeElementAt(row);
//        super.fireTableDataChanged();
//    }
//
//    public Vector getRow(int row) {
//        return (Vector)data.elementAt(row);
//    }
//
//    public void updateRow(Vector updateRow, int row) {
//        data.setElementAt(updateRow, row);
//        super.fireTableDataChanged();
//    }
//
//    public void insertFromDB(ResultSet rs) {
//
//        try{
//            ResultSetMetaData rsmd = rs.getMetaData();
//            cols = rsmd.getColumnCount();
//
//            colNames = new String[cols];
//            for (int i = 0; i < cols; i++) colNames[i] = rsmd.getColumnName(i+1);
//
//            data = new Vector();
//
//            while (rs.next()) {
//                Vector col = new Vector();
//                for (int i = 1; i <= cols; i++) col.addElement(rs.getObject(i));
//                data.addElement(col);
//            }
//
//            rs.close();
//
//        } catch (SQLException e) {e.printStackTrace();}
//
//        fireTableChanged(null);
//    }
//
//    public String getColumnName(int col) {
//        return colNames[col];
//    }
//
//    public Class getColumnClass(int col) {
//        return getValueAt(0, col).getClass();
//    }
//
//    public void clearTable() {
//        cols = 0;
//        data = new Vector();
//        colNames = null;
//        super.fireTableDataChanged();
//    }
//}