package com.baditsa.DistanceLearningSystem;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 29.04.15
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
public class Task implements Serializable {

    private Topic topic; // Ссылка на учебную
    private String[] question; // Набор вопросов
    String[][] answer; // Набор ответов на каждый вопрос
    int[] master; // Номера правильных ответов

    public Task() {
    }

    public Task(Topic topic, String[] question, String[][] answer, int[] master) {
        this.topic = topic;
        this.question = question;
        this.answer = answer;
        this.master = master;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String[] getQuestion() {
        return question;
    }

    public void setQuestion(String[] question) {
        this.question = question;
    }

    public String[][] getAnswer() {
        return answer;
    }

    public void setAnswer(String[][] answer) {
        this.answer = answer;
    }

    public int[] getMaster() {
        return master;
    }

    public void setMaster(int[] master) {
        this.master = master;
    }

    public int makeMark(int[] answer) {
        int mark = 0;
        for (int k = 0; k < master.length; k++)
            if (master[k] == answer[k]) mark++;
        return mark;
    }

    public String toString() {
        return "Task: " + topic;
    }
}