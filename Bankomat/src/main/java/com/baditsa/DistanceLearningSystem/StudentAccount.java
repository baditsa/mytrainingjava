package com.baditsa.DistanceLearningSystem;

import java.io.Serializable;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 29.04.15
 * Time: 13:32
 * To change this template use File | Settings | File Templates.
 */
public class StudentAccount implements Serializable {

    private Student stud; // Текущее состояние дел ученика
    private String name; // Имя ученика
    private String address; // Адрес ученика
    private Vector courses; // Список изученных курсов
    private Vector marks; // Список полученных оценок

    public StudentAccount() {
    }

    public StudentAccount(Student stud, String name, String address, Vector courses, Vector marks) {
        this.stud = stud;
        this.name = name;
        this.address = address;
        this.courses = courses;
        this.marks = marks;
    }

    public Student getStudent() {
        return stud;
    }

    public void setStudent(Student stud) {
        this.stud = stud;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Vector getCourses() {
        return courses;
    }

    public void setCourses(Vector courses) {
        this.courses = courses;
    }

    public Vector getMarks() {
        return marks;
    }

    public void setMarks(Vector marks) {
        this.marks = marks;
    }

    public void addCourse(Course newCourse) {
        courses.add(newCourse);
    }

    public void addMark(int newMark) {
        marks.add(new Integer(newMark));
    }

    public String toString() {
        return "StudentAccount: " + stud + " " + name + " " + address;
    }
}