package com.baditsa.DistanceLearningSystem;

import java.io.Serializable;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 29.04.15
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
public class Course implements Serializable {

    private String name; // Название курса
    private String description; // Описание курса
    private Vector topic; // Темы курса

    public Course() {
    }

    public Course(String name, String descr, Vector topic) {
        this.name = name;
        description = descr;
        this.topic = topic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return description;
    }

    public void setDescr(String descr) {
        description = descr;
    }

    public Vector getTopics() {
        return topic;
    }

    public void setTopics(Vector topic) {
        this.topic = topic;
    }

    public String toString() {
        return "Course: " + name;
    }
}