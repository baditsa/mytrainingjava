package com.baditsa.DistanceLearningSystem;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 29.04.15
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class Topic implements Serializable {

    private Course course; // Курс, к которому отностится тема
    private String content; // Учебный материал

    public Topic() {
    }

    public Topic(Course course, String content) {
        this.course = course;
        this.content = content;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String toString() {
        return "Topic: " + course + " " + content;
    }
}