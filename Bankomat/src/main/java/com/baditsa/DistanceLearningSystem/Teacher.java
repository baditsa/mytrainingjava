package com.baditsa.DistanceLearningSystem;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 29.04.15
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
public class Teacher implements Serializable {

    private String name; // Имя учителя
    private Course course; // Курс, который ведет учитель
    private Topic topic; // Тема, изменяемая учителем
    private Task task; // Вопросы, задаваемые учителем
    private Date date; // Дата последнего изменения курса

    public Teacher() {
    }

    public Teacher(String name, Course course, Topic topic, Task task, Date date) {
        this.name = name;
        this.course = course;
        this.topic = topic;
        this.task = task;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String toString() {
        return "Teacher: " + name + " " + course + " " + topic + " " + task + " " + date;
    }
}