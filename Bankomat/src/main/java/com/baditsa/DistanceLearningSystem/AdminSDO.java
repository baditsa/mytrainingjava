package com.baditsa.DistanceLearningSystem;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 08.05.15
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
public class AdminSDO extends JFrame {

    JPopupMenu popup = new JPopupMenu("Контекстное меню");

    public AdminSDO() {
        super(" Консоль администратора СДО");
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
        Container c = getContentPane();

        //---------------------------------Меню и панель инструментальных кнопок---------------------------------------
        JToolBar tb = new JToolBar();
        c.add(tb, BorderLayout.NORTH);

        JMenuBar mb = new JMenuBar();
        setJMenuBar(mb);

        //--------------------------------------Меню "Файл"------------------------------------------------------------
        JMenu menu = new JMenu("Файл");
        mb.add(menu);
        menu.setMnemonic(KeyEvent.VK_A);

        Action act = new AbstractAction("Открыть", new ImageIcon("open16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Открыть");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.CTRL_MASK));
            }

            public void actionPerformed(ActionEvent e) {
                JFileChooser fch = new JFileChooser("Открытие документа");
                fch.showOpenDialog(null);
            }
        };
        tb.add(act);
        menu.add(act);
        popup.add(act);

        act = new AbstractAction("Сохранить", new ImageIcon("Save16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Сохранить");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
            }

            public void actionPerformed(ActionEvent e) {
                // Запись в файл
                JOptionPane.showInputDialog(this, "Заглушка");
            }
        };
        tb.add(act);
        menu.add(act);
        popup.add(act);

        act = new AbstractAction("Сохранить как...", new ImageIcon("SaveAs16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Сохранить как");
            }

            public void actionPerformed(ActionEvent e) {
                JFileChooser fch = new JFileChooser("Сохранение документа");
                fch.showSaveDialog(null);
            }
        };
        tb.add(act);
        menu.add(act);
        popup.add(act);

        tb.addSeparator();
        menu.addSeparator();
        popup.addSeparator();

        JMenuItem exit = new JMenu("Выход");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menu.add(exit);

        //----------------------------------------Меню "Правка"--------------------------------------------------------
        mb.add(menu = new JMenu("Правка"));
        menu.setMnemonic(KeyEvent.VK_G);

        act = new AbstractAction("Вырезать", new ImageIcon("Cut16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Вырезать");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK));
            }

            public void actionPerformed(ActionEvent e) {
                // Вырезать
                JOptionPane.showInputDialog(this, "Заглушка");
            }
        };
        tb.add(act);
        menu.add(act);
        popup.add(act);

        act = new AbstractAction("Копировать", new ImageIcon("Copy16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Скопировать");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK));
            }

            public void actionPerformed(ActionEvent e) {
                // Скопировать
                JOptionPane.showInputDialog(this, "Заглушка");
            }
        };
        tb.add(act);
        menu.add(act);
        popup.add(act);

        act = new AbstractAction("Вставить", new ImageIcon("Paste16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Вставить");
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK));
            }

            public void actionPerformed(ActionEvent e) {
                // Вставить
                JOptionPane.showInputDialog(this, "Заглушка");
            }
        };
        tb.add(act);
        menu.add(act);
        popup.add(act);

        tb.addSeparator();

        //--------------------------------------------Меню "База"------------------------------------------------------
        mb.add(menu = new JMenu("База"));
        menu.setMnemonic(KeyEvent.VK_F);
        JMenuItem conf = menu.add("Конфигурация...");
        conf.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showInputDialog(this, "Заглушка");
            }
        });

        //--------------------------------------------Меню "Отчет"-----------------------------------------------------
        mb.add(menu = new JMenu("Отчет"));
        menu.setMnemonic(KeyEvent.VK_J);
        JMenuItem stud = menu.add("Ученики");
        JMenuItem stCourses = menu.add("Предметы");
        JMenuItem courses = menu.add("Курсы");
        JMenuItem topics = menu.add("Темы");
        JMenuItem tasks = menu.add("Задачи");
        JMenuItem teacher = menu.add("Учителя");

        //--------------------------------------------Меню "Справка"---------------------------------------------------
        mb.add(menu = new JMenu("Справка"));
        menu.setMnemonic(KeyEvent.VK_C);

        act = new AbstractAction("Справка", new ImageIcon("Help16.gif")) {
            {
                putValue(Action.SHORT_DESCRIPTION, "Справка");
            }

            public void actionPerformed(ActionEvent e) {
                // Вызов справки
                JOptionPane.showMessageDialog(null, "Заглушка");
            }
        };
        tb.add(act);
        menu.add(act);
        menu.addSeparator();

        JMenuItem about = menu.add("О программе");
        about.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Заглушка");
            }
        });
        //---------------------------------------Конец описания меню---------------------------------------------------

        //-----------------------------------------Двойная панель------------------------------------------------------
        JSplitPane sp = new JSplitPane();
        sp.setOneTouchExpandable(true);
        c.add(sp);

        //----------------------------------------Текстовая область----------------------------------------------------
        final JTextArea ta = new JTextArea();
        sp.setRightComponent(new JScrollPane(ta));

        //---------------------------------------------Дерево----------------------------------------------------------
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("База СДО");

        JTree tr = new JTree(root);
        sp.setLeftComponent(new JScrollPane(tr));

        DefaultMutableTreeNode students = new DefaultMutableTreeNode("Студенты");
        root.add(students);

        DefaultMutableTreeNode studIds = new DefaultMutableTreeNode("Сведения");
        students.add(studIds);

        DefaultMutableTreeNode studCourses = new DefaultMutableTreeNode("Курсы");
        students.add(studCourses);

        DefaultMutableTreeNode teach = new DefaultMutableTreeNode("Учителя");
        root.add(teach);

        DefaultMutableTreeNode teachCourses = new DefaultMutableTreeNode("Курсы");
        teach.add(teachCourses);

        DefaultMutableTreeNode tsks = new DefaultMutableTreeNode("Задачи");
        root.add(tsks);

        tr.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                TreePath[] path = e.getPaths();
                for (int i = 0; i < path.length; i++) {
                    ta.append(path[i].getLastPathComponent() + "\n");
                }
            }
        });

        //------------------------------------------Таблица------------------------------------------------------------
        String[][] data = {
                {"Иванов", "Кулинария", "25 июля 2001"},
                {"Петров", "Астрология", "10 декабря 2001"},
                {"Сидоров", "Уфология", "23 октября 2001"},
                {"Токарев", "Философия", "15 ноября 2001"}};
        String[] head = {"Фамилия", "Название", "Дата"};

        JTable tbl = new JTable(data, head);
        sp.setRightComponent(new JScrollPane(tbl));

        //--------------------------------------Конец оформления-------------------------------------------------------

        LoginWin lw = new LoginWin(this);
        lw.setVisible(true);

        setSize(500, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void processMouseEvent(MouseEvent e) {
        if (e.isPopupTrigger()) popup.show(e.getComponent(), e.getX(), e.getY());
        else super.processMouseEvent(e);
    }

    public static void main(String[] args) {
        new AdminSDO();
    }
}

class LoginWin extends JDialog {
    LoginWin(JFrame fr) {
        super(fr, "Окно входа", true);
        Container c = getContentPane();
        c.add(new JLabel("Вход в утилиту администратора СДО"), BorderLayout.NORTH);

        JPanel p1 = new JPanel(false);
        p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
        c.add(p1);

        JPanel p2 = new JPanel(false);
        JButton b1 = new JButton("Войти");
        JButton b2 = new JButton("Отменить");
        p2.add(b1);
        p2.add(b2);
        c.add(p2, BorderLayout.SOUTH);

        JPanel p3 = new JPanel(false);
        p3.setLayout(new GridLayout(0, 1));
        p1.add(p3);

        p3.add(new JLabel("Имя: ", JLabel.RIGHT));
        p3.add(new JLabel("Пароль: ", JLabel.RIGHT));

        JPanel p4 = new JPanel(false);
        p4.setLayout(new GridLayout(0, 1));
        p1.add(p4);

        JTextField name = new JTextField(10);
        p4.add(name);

        JPasswordField pass = new JPasswordField(10);
        p4.add(pass);

        pack();
    }
}