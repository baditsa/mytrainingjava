package com.baditsa.Movie;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 13.03.15
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
class Movie {
    String title;
    String genre;
    int rating;

    void playIT() {
        System.out.println("Проигрывание фильма");
    }
}

class MovieTestDrive {
    public static void main(String[] args) {
        Movie one = new Movie();
        one.title = "Как прогореть на акциях";
        one.genre = "Трагедия";
        one.rating = -2;

        Movie two = new Movie();
        two.title = "Потерянные в офисе";
        two.genre = "Комедия";
        two.rating = 5;

        Movie three = new Movie();
        three.title = "Байт-Клуб";
        three.genre = "Веселая трагедия";
        three.rating = 127;
    }
}