package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 05.05.15
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
public class SimpTable extends JFrame {

    SimpTable() {
        super("My Table");
        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        String[][] data = {{"-27", "32"}, {"-45", "55"}};
        String[] colNames = {"Вчера", "Сегодня"};

        JTable t1 = new JTable(data, colNames);

        c.add(new JScrollPane(t1));

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new SimpTable();
    }
}
