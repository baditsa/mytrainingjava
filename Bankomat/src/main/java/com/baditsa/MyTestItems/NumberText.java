package com.baditsa.MyTestItems;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 05.05.15
 * Time: 12:17
 * To change this template use File | Settings | File Templates.
 */
public class NumberText extends JFrame implements ActionListener {
    JTextField tf = new JTextField(5);
    JLabel l = new JLabel("Вводите цифры");

    NumberText() {
        super("text");
        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        ((PlainDocument) tf.getDocument()).setDocumentFilter(new NumberFilter()); // Вставляем фильтр вводимых символов

        tf.setSelectedTextColor(Color.red); // Текст будет выделяться только красным цветом

        tf.setSelectionColor(Color.white); // При выделении текста фон останется белым

        tf.setCaretColor(Color.red); // Курсор будет красным

        tf.addActionListener(this); // Присоединяем обработчик событий
        l.setLabelFor(tf);

        c.add(l);
        c.add(tf);

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        l.setText(tf.getText());
    }

    public static void main(String[] args) {
        new NumberText();
    }

    class NumberFilter extends DocumentFilter { // Фильтр вводимых данных

        public void insertString(FilterBypass fb, int pos, String text, AttributeSet attr) throws BadLocationException {
            try {
                Integer.parseInt(text); // Введена цифра?
            } catch (Exception e) {
                super.insertString(fb, 0, "", attr); // Если не цифра, то символ не вводим
                return;
            }
            super.insertString(fb, pos, text, attr); // Если цифра - заносим ее в поле
        }
    }
}
