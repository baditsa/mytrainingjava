package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 08.05.15
 * Time: 13:26
 * To change this template use File | Settings | File Templates.
 */
public class SimpTimer extends JFrame implements ActionListener {
    JButton bt;
    JTextArea ta;
    javax.swing.Timer t;

    SimpTimer() {
        super(" Передача фокуса");
        Container c = getContentPane();
        JPanel p = new JPanel();
        c.add(p, BorderLayout.NORTH);

        bt = new JButton("Останов");
        bt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                t.stop();
                ta.append("\nStop");
            }
        });

        ta = new JTextArea(6, 30);

        c.add(ta);
        p.add(bt);

        t = new javax.swing.Timer(500, this);
        t.setInitialDelay(1000);
        t.setLogTimers(true);
        t.start();

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        ta.append("\n Время: " + new Date());
    }

    public static void main(String[] args) {
        new SimpTimer();
    }
}
