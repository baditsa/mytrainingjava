package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 08.05.15
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
public class LabelDnD extends JFrame {

    public LabelDnD() {
        super(" Перенос текста в надпись JLabel");

        JTextField tf = new JTextField(100);
        tf.setDragEnabled(true);

        JLabel tl = new JLabel("Перетащи сюда", JLabel.LEFT);
        tl.setTransferHandler(new TransferHandler("text"));
        tl.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                JComponent c = (JComponent) e.getSource();
                TransferHandler th = c.getTransferHandler();
                th.exportAsDrag(c, e, TransferHandler.COPY);
            }
        });

        getContentPane().add(tf, BorderLayout.NORTH);
        getContentPane().add(tl);

        tl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(200, 100);
        setVisible(true);
    }

    public static void main(String[] args) {
        new LabelDnD();
    }
}
