package com.baditsa.MyTestItems;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 21.04.15
 * Time: 13:07
 * To change this template use File | Settings | File Templates.
 */
public class Spreadsheet2 extends JFrame {
    Spreadsheet2() {
        super("Test Spreadsheet 2");
        JCheckBox checkBox = new JCheckBox();
        checkBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("e = " + e);

            }
        });
        JTable t1 = new JTable(new FirstColumnTableModel());
        t1.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//        t1.setDefaultEditor(Boolean.class, new DefaultCellEditor(checkBox));

        getContentPane().add(new JScrollPane(t1));

//        setSize(400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        new Spreadsheet2();
    }
}

class FirstColumnTableModel extends DefaultTableModel {
    //    Object[][] data = {
//            {"Bass Drum", new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false), new Boolean(false),},
//            {"Closed Hi-Hat", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Open Hi-Hat", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Acoustic Snare", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Crash Cymbal", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Hand Clap", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"High Tom", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Hi Bongo", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Maracas", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Whistle", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Low Conga", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Cowbell", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Vibraslap", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Low-mid Tom", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"High Agogo", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
//            {"Open Hi Conga", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
    private final static Object[] colNames = {"Instruments", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    private static Object[][] data = new Object[16][colNames.length];

    //    };
    String[] rowNames1 = {"Bass Drum", "Closed Hi-Hat", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};

    public FirstColumnTableModel() {
        super(data, colNames);
        for (int i = 0; i < data.length; i++) {
            Object[] row = data[i];
            for (int j = 0; j < row.length; j++) {
                if (j == 0) {
                    row[j] = rowNames1[i];
                } else {
                    row[j] = new Boolean(false);
                }


            }

        }

    }

    public int getRowCount() {
        return data.length;
    }

    public int getColumnCount() {
        return data[0].length;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    public Class getColumnClass(int col) {
        if (col == 0) return String.class;
        else return Boolean.class;
    }

    public String getColumnName(int col) {
        return String.valueOf(colNames[col]);
    }
}


