/*
package com.baditsa.MyTestItems;

import sun.plugin2.message.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 14.05.15
 * Time: 16:01
 * To change this template use File | Settings | File Templates.
 *//*

public class Client extends JFrame implements MessageReceiver, ActionListener {

    JTextField tf;
    JTextArea ta;
    static String name;
    static MessageServer server;

    public Client() throws RemoteException{
        setTitle(name);
        Container c = getContentPane();

        c.add(new JLabel("Обмен сообщениями", JLabel.CENTER), BorderLayout.NORTH);

        tf = new JTextField(30);
        tf.addActionListener(this);
        c.add(tf, BorderLayout.SOUTH);

        ta = new JTextArea(10, 30);
        c.add(ta);

        UnicastRemoteObject.exportObject(this);

        setSize(400, 300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void actionPerformed(ActionEvent ae) {
        try{
            server.send(name, tf.getText());
        } catch (Exception e){
            System.err.println(e);
        }
        tf.setText("");
    }

    public void print(String mess){
        ta.append(mess + "\n");
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: java Client name");
            System.exit(-1);
        }

        name = args[0];

        if(System.getSecurityManager() == null)
            System.setSecurityManager(new RMISecurityManager());

        try{
            Client cl = new Client();

            server = (MessageServer) Naming.lookup("//hostname/chat");

            int n = server.register(name, cl);

            if (n == MessageServer.FAILURE)
                throw new Exception("Couldn't connect to server);
        }catch (Exception e){
            System.err.println(e);
            System.exit(-1);
        }
    }
}*/
