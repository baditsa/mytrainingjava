package com.baditsa.MyTestItems;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 21.04.15
 * Time: 13:07
 * To change this template use File | Settings | File Templates.
 */
public class Spreadsheet1 extends JFrame {
    public static void createGUI() {
        JFrame frame = new JFrame("Test Spreadsheet 1");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        String[] columnNames = {"Instruments", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"}; // Собираем столбцы в массив строк

        Object[][] data = { // Определяем данные для отображения в таблице также при помощи массива строк
                {"Bass Drum", new Boolean(false), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Closed Hi-Hat", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Open Hi-Hat", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Acoustic Snare", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Crash Cymbal", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Hand Clap", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"High Tom", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Hi Bongo", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Maracas", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Whistle", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Low Conga", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Cowbell", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Vibraslap", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Low-mid Tom", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"High Agogo", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
                {"Open Hi Conga", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",},
        };

        JTable table = new JTable(data, columnNames); // Создать собственно сам JTable и передаем ему массив столбцов и массив с данным для показа

        JScrollPane scrollPane = new JScrollPane(table); // Создаем JScrollPane, на котором размещаем таблицу и добавляем панель на JFrame

        frame.getContentPane().add(scrollPane);
//        frame.setPreferredSize(new Dimension(450, 200)); // Устанавливаем размер
        frame.pack(); // ???
//      frame.setLocationRelativeTo(null); // Расположение относительно ???
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() { // ???
            public void run() {
                // JFrame.setDefaultLookAndFeelDecorated(false); // ???
                createGUI();
            }
        });
    }
}