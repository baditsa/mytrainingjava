package com.baditsa.MyTestItems;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 06.05.15
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
public class MultiLineTable extends JFrame {
    MultiLineTable(int lineCount) {
        super("Таблица с многострочными ячейками");

        DefaultTableModel tm = new DefaultTableModel() { // Расширяем модель ячеек,

            public Class getColumnClass(int col) {       // переопределяя метод,
                return getValueAt(0, col).getClass();    // возвращающий класс содержимого столбца.
            }
        };

        tm.setDataVector(                                // Заносим в модель ячеек данные.
                new Object[][]{
                        {"Имя\nФамилия", "Иван\nПетров", "Петр\nИванов"},
                        {"Отдел\nДолжность", "Сбыт\nВодитель", "Сбыт\nЭкспедитор"},
                },
                new Object[]{"Данные", "1", "2"}
        );

        JTable t = new JTable(tm); // Создаем таблицу с новой моделью ячеек.

        t.setRowHeight(t.getRowHeight() * lineCount); // Изменяем высоту ячеек на экране, чтобы поместились все строки содержимого ячейки

        t.setDefaultRenderer(String.class, new MultiLineCellRenderer()); // Устанавливаем новый класс-рисовальщик

        getContentPane().add(new JScrollPane(t));

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MultiLineTable(2);
    }
}

class MultiLineCellRenderer extends JTextArea implements TableCellRenderer { // Класс-рисовальщик

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        if (hasFocus) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if (table.isCellEditable(row, col)) {
                setForeground(UIManager.getColor("Table.focusCellForeground"));
                setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } else setBorder(new EmptyBorder(1, 2, 1, 2));

        setFont(table.getFont());
        setText((value == null) ? "" : value.toString());

        return this;
    }
}