package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 06.05.15
 * Time: 14:51
 * To change this template use File | Settings | File Templates.
 */
public class MyBox extends JFrame {

    JButton b1 = new JButton("Первая");
    JButton b2 = new JButton("Вторая");
    JTextArea ta = new JTextArea(5, 30);

    MyBox() {
        super("Линейная панель");
        Container c = getContentPane();
        c.setLayout(new FlowLayout());

        Box out = Box.createVerticalBox();
        Box in1 = Box.createHorizontalBox();
        Box in2 = Box.createHorizontalBox();

        out.add(in1);
        out.add(in2);

        in1.add(ta);

        in2.add(Box.createHorizontalGlue());
        in2.add(b1);
        in2.add(b2);

        c.add(out);

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MyBox();
    }
}
