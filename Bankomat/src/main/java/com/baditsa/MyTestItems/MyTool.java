package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 06.05.15
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
public class MyTool extends JFrame {

    MyTool() {
        super(" Инструментальные панели");
        Container c = getContentPane();

        JToolBar tb1 = new JToolBar(" Панель 1");
        JToolBar tb2 = new JToolBar(" Панель 2");

        tb1.setRollover(true);

        tb1.add(new JButton(new ImageIcon("Add24.gif")));
        tb1.add(new JButton(new ImageIcon("AlignTop24.gif")));
        tb1.add(new JButton(new ImageIcon("About24.gif")));

        tb2.add(new JButton("Первая"));
        tb2.add(new JButton("Вторая"));
        tb2.add(new JButton("Третья"));

        c.add(tb1, BorderLayout.NORTH);
        c.add(tb2, BorderLayout.WEST);

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MyTool();
    }
}