package com.baditsa.MyTestItems;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 06.05.15
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */
public class HeadTable extends JFrame {
    HeadTable() {
        super("My Table");
        Container c = getContentPane();

        Vector data = new Vector();
        Vector row = new Vector();

        row.addElement("Иванов");
        row.addElement(new Integer(1970));
        row.addElement(new Boolean(false));

        //data.addElement(r);

        row = new Vector();

        row.addElement("Петров");
        row.addElement(new Integer(1980));
        row.addElement(new Boolean(true));

        //data.addElement(r);

        Vector col = new Vector();

        col.addElement("Фамилия");
        col.addElement("Год рождения");
        col.addElement("Семейное положение");

        JTable t2 = new JTable(data, col);

        JTableHeader th = t2.getTableHeader();

        c.add(th, BorderLayout.SOUTH);
        c.add(t2, BorderLayout.CENTER);

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new HeadTable();
    }
}