package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 06.05.15
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public class SpringWin extends JFrame {
    JComponent[] comp = {
            new JButton("Длинная надпись"),
            new JButton("<html>Надпись с<p> двумя строками"),
            new JButton("OK")
    };

    public SpringWin() {
        super(" Размещение SpringLayout");
        Container c = getContentPane();
        SpringLayout sl = new SpringLayout();
        c.setLayout(sl);

        Spring xPad = Spring.constant(6); // Задаем величину промежутка между компонентами

        Spring yPad = Spring.constant(10); // Задаем величину отступа от границ контейнера

        Spring currX = yPad; // Текущее положение левого верхнего угла

        Spring maxHeight = Spring.constant(0); // Наибольшая высота компонента, пока 0

        for (int i = 0; i < comp.length; i++) {
            c.add(comp[i]);
            SpringLayout.Constraints cons = sl.getConstraints(comp[i]); // Получаем размер i-го компонента
            cons.setX(currX);                                           // Устанавливаем положение i-го компонента
            cons.setY(yPad);
            currX = Spring.sum(xPad, cons.getConstraint("East"));       // Перемещаем текущее положение угла
            maxHeight = Spring.max(maxHeight, cons.getConstraint("South")); // Изменяем наибольшую высоту
        }

        SpringLayout.Constraints pCons = sl.getConstraints(c); // Получаем размеры контейнера
        pCons.setConstraint(SpringLayout.EAST, Spring.sum(currX, yPad)); // Устанавливаем размеры
        pCons.setConstraint(SpringLayout.SOUTH, Spring.sum(maxHeight, yPad)); // всего содержимого контейнера

        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new SpringWin();
    }
}
