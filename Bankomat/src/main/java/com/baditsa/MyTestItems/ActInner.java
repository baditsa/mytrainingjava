package com.baditsa.MyTestItems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 07.05.15
 * Time: 12:15
 * To change this template use File | Settings | File Templates.
 */
public class ActInner extends JFrame {
    private JButton b = new JButton("Миллисекунды");
    private JLabel label = new JLabel("Отсчет", JLabel.RIGHT);
    private long msecs = System.currentTimeMillis();

    ActInner() {
        super(" Отсчет времени");
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        c.add(label);
        b.setMnemonic(KeyEvent.VK_L);
        c.add(b);

        MilliSeconds ms = new MilliSeconds(msecs);
        b.addActionListener((ActionListener) ms);

        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new ActInner();
    }

    private class MilliSeconds implements ActionListener {

        private long msecs, old;

        public MilliSeconds(long msecs) {
            this.msecs = old = msecs;
        }

        public void actionPerformed(ActionEvent ae) {
            if (ae.getActionCommand().equals("Миллисекунды")) {
                msecs = ae.getWhen();
                label.setText("" + (msecs - old));
                old = msecs;
            }
        }
    }
}
