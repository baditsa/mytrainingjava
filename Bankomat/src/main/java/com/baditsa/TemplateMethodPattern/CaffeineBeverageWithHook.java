package com.baditsa.TemplateMethodPattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 15.07.15
 * Time: 19:59
 * To change this template use File | Settings | File Templates.
 */
public abstract class CaffeineBeverageWithHook {
    final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            addCondiments();
        }
    }

    abstract void brew();

    abstract void addCondiments();

    public void boilWater() {
        System.out.println("Boiling water");
    }

    public void pourInCup() {
        System.out.println("Pouring into cup");
    }

    private boolean customerWantsCondiments() {
        return true;
    }
}