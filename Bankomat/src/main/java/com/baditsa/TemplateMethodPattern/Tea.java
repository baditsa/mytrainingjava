package com.baditsa.TemplateMethodPattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 15.07.15
 * Time: 18:52
 * To change this template use File | Settings | File Templates.
 */
public class Tea extends CaffeineBeverage {

    @Override
    void brew() {
        System.out.println("Steeping the tea");
    }

    @Override
    void addCondiments() {
        System.out.println("Adding Lemon");
    }
}