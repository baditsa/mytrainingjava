package com.baditsa.TemplateMethodPattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 15.07.15
 * Time: 18:42
 * To change this template use File | Settings | File Templates.
 */
public class Coffee extends CaffeineBeverage {

    @Override
    void brew() {
        System.out.println("Dripping Coffee through filter");
    }

    @Override
    void addCondiments() {
        System.out.println("Adding Sugar and Milk");
    }
}