package com.baditsa.Pult;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 13.07.15
 * Time: 14:37
 * To change this template use File | Settings | File Templates.
 */
public class CeilingFan {
    public static final int HIGH = 3;
    public static final int MEDIUM = 2;
    public static final int LOW = 1;
    public static final int OFF = 0;
    String location;
    int speed; // class CeilingFan has a local variable of state, which representing fan rotational speed

    public CeilingFan(String location) {
        this.location = location;
        speed = OFF;
    }

    public void high() {
        speed = HIGH;
    }

    public void medium() {
        speed = MEDIUM;
    }

    public void low() {
        speed = LOW;
    }

    public void off() {
        speed = OFF;
    }

    public int getSpeed() { // method getSpeed() used to get the current speed of the fan
        return speed;
    }
}