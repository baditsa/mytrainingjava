package com.baditsa.Pult;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 13.07.15
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
public class MacroCommand implements Command {
    Command[] commands;

    public MacroCommand(Command[] commands) {
        this.commands = commands; // take an array of commands and save them in the object MacroCommand
    }

    public void execute() {
        for (int i = 0; i < commands.length; i++) {
            commands[i].execute(); // With the macrocommand all commands will be executed sequentially
        }
    }
}
