/*
package com.baditsa.Pult;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 09.07.15
 * Time: 18:32
 * To change this template use File | Settings | File Templates.
 *//*

// Каждая операция, активизируемая нажатием кнопки на пульте, реализуется простым объектом команды.
// Объект команды хранит ссылку на экземпляр класса устройства, реализующий метод execute(), который вызывает один
// или несколько методов объекта.
public class LightOnCommand implements Command {
    Light light;

    // В переменной light конструктору передается конкретный объект, которым будет управлять команда
    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.on();
    }

    public void undo() {
        light.off();
    }
}*/
