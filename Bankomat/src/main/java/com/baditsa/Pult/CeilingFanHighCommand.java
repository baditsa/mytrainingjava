package com.baditsa.Pult;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 13.07.15
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
public class CeilingFanHighCommand implements Command {
    CeilingFan ceilingFan;
    int prevSpeed; // add a local variable of state to store the previous speed

    public CeilingFanHighCommand(CeilingFan ceilingFan) {
        this.ceilingFan = ceilingFan;
    }

    public void execute() {
        prevSpeed = ceilingFan.getSpeed(); // in method execute() before changing speed its previous value saved for possible cancellation
        ceilingFan.high();
    }

    public void undo() { // in method undo() fan returns to the previous speed
        if (prevSpeed == CeilingFan.HIGH) {
            ceilingFan.high();
        } else if (prevSpeed == CeilingFan.MEDIUM) {
            ceilingFan.medium();
        } else if (prevSpeed == CeilingFan.LOW) {
            ceilingFan.low();
        } else if (prevSpeed == CeilingFan.OFF) {
            ceilingFan.off();
        }
    }
}
