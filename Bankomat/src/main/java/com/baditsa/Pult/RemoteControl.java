/*
package com.baditsa.Pult;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 09.07.15
 * Time: 19:34
 * To change this template use File | Settings | File Templates.
 *//*

// Управляет набором объектов команд (по одному на кнопку). При нажатии кнопки вызывается соответствующий метод ButtonWasPushed(),
// который активизирует метод execute() объекта команды. Класс пульта больше ничего не знает о тех классах, к которым
// он обращается, т.к. он отделен от них объектом команды
public class RemoteControl {
    Command[] onCommands;
    Command[] offCommands;

    public RemoteControl(){
        onCommands = new Command[7];
        offCommands = new Command[7];

        Command noCommand = new NoCommand();
        for (int i = 0; i < 7; i++) {
            onCommands[i]  = noCommand;
            offCommands[i]  = noCommand;
        }
    }

    // Метод setCommand() получает ячейку и команды включения/выключения для этой ячейки.
    // Команды сохраняются в массивах для последующего использования
    public void setCommand (int slot, Command onCommand, Command offCommand) {
        onCommands[slot] = onCommand;
        offCommands[slot] = offCommand;
    }

    public void onButtonWasPushed(int slot) {
        onCommands[slot].execute();
    }

    public void offButtonWasPushed(int slot) {
        offCommands[slot].execute();
    }

    @Override
    public String toString() {
        StringBuffer stringBuff = new StringBuffer();
        stringBuff.append("\n------ Remote Control ------\n");
        for (int i = 0; i < onCommands.length; i++) {
            stringBuff.append("[slot " + i + "] " + onCommands[i].getClass().getName() + " " + offCommands[i].getClass().getName() + "\n")
        }
        return stringBuff.toString();
    }
}*/
