package com.baditsa.Pult;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 09.07.15
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */
public class SimpleRemoteControl {
    Command slot;

    public SimpleRemoteControl() {
    }

    // метод для назначения команды
    public void setCommand(Command command) {
        slot = command;
    }

    // метод, вызываемый при нажатии кнопки
    public void buttonWasPressed() {
        slot.execute();
    }
}