/*
package com.baditsa.Pult;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 13.07.15
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 *//*

public class RemoteControlWithUndo {
    Command[] onCommands;
    Command[] offCommands;
    Command undoCommand; // переменная для хранения последней выполненной команды

    public RemoteControlWithUndo(){
        onCommands = new Command[7];
        offCommands = new Command[7];

        Command noCommand = new NoCommand();
        for (int i = 0; i < 7; i++) {
            onCommands[i]  = noCommand;
            offCommands[i]  = noCommand;
        }
        undoCommand = noCommand; // в переменную undoCommand изначально также заносится объект noCommand, чтобы при нажатии кнопки отмены ранее любых других кнопок ничего не происходило
    }

    public void setCommand (int slot, Command onCommand, Command offCommand) {
        onCommands[slot] = onCommand;
        offCommands[slot] = offCommand;
    }

    public void onButtonWasPushed(int slot) {
        onCommands[slot].execute();
        undoCommand = onCommands[slot]; // при нажатии кнопки мы сначала читаем команду и выполняем её, а затем сохраняем ссылку на нее в переменной undoCommand
    }

    public void offButtonWasPushed(int slot) {
        offCommands[slot].execute();
        undoCommand = offCommands[slot]; // при нажатии кнопки мы сначала читаем команду и выполняем её, а затем сохраняем ссылку на нее в переменной undoCommand
    }

    public void undoButtonWasPushed() { // при нажатии кнопки отмены мы вызываем метод undo() команды, хранящейся в переменной undoCommand. Вызов отменяет операцию последней выполненной команды
        undoCommand.undo();
    }

    @Override
    public String toString() {
        StringBuffer stringBuff = new StringBuffer();
        stringBuff.append("\n------ Remote Control ------\n");
        for (int i = 0; i < onCommands.length; i++) {
            stringBuff.append("[slot " + i + "] " + onCommands[i].getClass().getName() + " " + offCommands[i].getClass().getName() + "\n")
        }
        return stringBuff.toString();
    }
}*/
