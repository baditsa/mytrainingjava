package com.baditsa.GoodDog;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 17.03.15
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
class GoodDog {
    private int size; // Помечаем переменную экземпляра как private

    public int getSize() { // Помечаем геттеры как public
        return size;
    }

    public void setSize(int s) { // Помечаем сеттеры как public
        size = s;
    }

    void bark() {
        if (size > 60) {
            System.out.println("Гав Гав!");
        } else if (size > 14) {
            System.out.println("Вуф Вуф!");
        } else {
            System.out.println("Тяф Тяф!");
        }
    }
}

class GoodDogTestDrive {
    public static void main(String[] args) {
        GoodDog one = new GoodDog();
        one.setSize(70);
        GoodDog two = new GoodDog();
        two.setSize(8);
        System.out.println("Первая собака: " + one.getSize());
        System.out.println("Вторая собака: " + two.getSize());
        one.bark();
        two.bark();
    }
}
