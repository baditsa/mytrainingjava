/*
package com.baditsa.BeatModel;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.Sequencer;
import java.util.ArrayList;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 05.08.15
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 *//*

public class BeatModel implements BeatModelInterface, MetaEventListener {
    Sequencer sequencer; // Генератор ритма
    ArrayList beatObservers = new ArrayList(); // В контейнерах ArrayList хранятся две категории наблюдателей
    ArrayList bpmObservers = new ArrayList();
    int bpm = 90; // В переменной bpm хранится частота ритма - по умолчанию 90 BPM

    @Override
    public void initialize() { // Метод настраивает генератор и готовит музыку для воспроизведения
        setUpMidi();
        buildTrackAndStart();
    }

    @Override
    public void on() { // Метод запускает генератор и устанавливает частоту по умолчанию (90 BPM)
        sequencer.start();
        setBPM(90);
    }

    @Override
    public void off() { // Метод останавливает генератор  и задает частоту равной 0.
        setBPM(0);
        sequencer.stop();
    }

    @Override
    public void setBPM(int bpm) { // Метод используется контроллером для управления ритмом. Он выполняет 3 операции:
        this.bpm = bpm; // 1. Присваивание значения переменной bpm.
        sequencer.setTempoInBPM(getBPM()); // 2. Запрос к генератору на изменение частоты.
        notifyBPMObservers(); // 3. Оповещение всех BPM-наблюдателей об изменении частоты.
    }

    @Override
    public int getBPM() { // Метод возвращает значение переменной bpm, определяющее текущую частоту ритма
        return bpm;
    }

    void beatEvent() {
        notifyBeatObservers();
    }

    @Override
    public void registerObserver(BeatObserver o) {
    }

    @Override
    public void removeObserver(BeatObserver o) {
    }

    @Override
    public void registerObserver(BPMObserver o) {
    }

    @Override
    public void removeObserver(BPMObserver o) {
    }

    @Override
    public void meta(MetaMessage metaMessage) {
    }
}*/
