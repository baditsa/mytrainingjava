/*
package com.baditsa.BeatModel;

*/
/*
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

*//*


import java.io.IOException;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 06.08.15
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 *//*
*/
/*

public class DJView implements ActionListener, BeatOserver, BPMObserver {
    BeatModelInterface model;
    ControllerInterface controller;
    JFrame viewFrame;
    JPanel viewPanel;
    BeatBar beatBar;
    JLabel bpmOutputLabel;

    JLabel bpmLabel;
    JTextField bpmTextField;
    JButton setBPMButton;
    JButton increaseBPMButton;
    JButton decreaseBPMButton;
    JMenuBar menuBar;
    JMenu menu;
    JMenuItem startMenuItem;
    JMenuItem stopMenuItem;

    public DJView(ControllerInterface controller, BeatModelInterface model) {
        this.controller = controller; // Конструктор получает ссылки на контроллер и модель и сохраняет их в переменных
        this.model = model;
        model.registerObserver((BeatObserver)this); // Представление регистрируется в качестве наблюдателя BeatObserver
        model.registerObserver((BPMObserver)this);  // и BPMObserver модели
    }

    public void createView() {
    }

    public void updateBPM() { // Метод updateBPM() вызывается при изменении состояния модели. Изображение обновляется текущим значением частоты, которое запрашивается непосредственно у модели
        int bpm = model.getBPM();
        if (bpm == 0) {
            bpmOutputLabel.setText("offline");
        } else {
            bpmOutputLabel.setText("Current BPM: " + model.getBPM());
        }
    }

    public void updateBeat() { // Метод updateBeat() вызывается в начале нового удара. Когда это происходит необходимо отобразить импульс на "индикаторе ритма".
        beatBar.setValue(100); // Для этого индикатору присваивается максимально возможное значение (100).
    }

    public void createControls() { // Метод создает все элементы управления и размещает их в интерфейсе приложения, а также создает меню
    }                              // При выборе команды Start или Stop вызываются соответствующие методы контроллера.

    // Методы установления и снятия блокировки команд меню Start и Stop.
    public void enableStopMenuItem() {
        stopMenuItem.setEnabled(true);
    }

    public void disableStopMenuItem() {
        stopMenuItem.setEnabled(false);
    }

    public void enableStartMenuItem() {
        startMenuItem.setEnabled(true);
    }

    public void disableStartMenuItem() {
        startMenuItem.setEnabled(false);
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == setBPMButton) { // Если выбрана кнопка Set, кнопка уменьшения или увеличения частоты  - информация передается контроллеру
            int bpm = Integer.parseInt(bpmTextField.getText());
            controller.setBPM(bpm);
        } else if (event.getSource() == increaseBPMButton) {
            controller.increaseBPM();
        } else if (event.getSource() == decreaseBPMButton) {
            controller.decreaseBPM();
        }
    }
}*//*


public class DJView extends HttpServlet {

    public void init() throws ServletException { // Метод init() вызывается при создании сервлета
        BeatModel beatModel = new BeatModel(); // Сначала создаем объект BeatModel...
        beatModel.initialize();
        getServletContext().setAttribute("beatModel", beatModel); //... и сохраняем ссылку на него в контексте сервлета для удобства дальнейших обращений.
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        BeatModel beatModel = (BeatModel)getServletContext().getAttribute("beatModel"); // Получаем ссылку на модель из контекста сервлета.
        String bpm = request.getParameter("bpm"); // Затем получаем все команды/параметры HTTP
        if (bpm == null) {
            bpm = beatModel.getBPM() + "";
        }

        String set = request.getParameter("set"); // Если получена команда записи, получаем присваиваемое значение  и сообщаем модели
        if (set != null) {
            int bpmNumber = 90;
            bpmNumber = Integer.parseInt(bpm);
            beatModel.setBPM(bpmNumber);
        }

        String decrease = request.getParameter("decrease"); // Получаем текущую частоту от модели и уменьшаем/увеличиваем  на 1
        if (decrease != null) {
            beatModel.setBPM(beatModel.getBPM() - 1);
        }

        String increase = request.getParameter("increase");
        if (increase != null) {
            beatModel.setBPM(beatModel.getBPM() + 1);
        }

        String on = request.getParameter("on"); // Если получена команда запуска или остановки, выполняем соответствующее действие с моделью
        if (on != null) {
            beatModel.start();
        }

        String off = request.getParameter("off");
        if (off != null) {
            beatModel.stop();
        }

        request.setAttribute("beatModel", beatModel); // Работа контроллера закончена. Передаем управление предоставлению, чтобы оно сгенерировало код HTML
        RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/DJView.jsp");
        dispatcher.forward(request, response);
    }
}*/
