/*
package com.baditsa.BeatModel;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 07.08.15
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 *//*

public class BeatController implements ControllerInterface {
    BeatModelInterface model; // Контроллер получает объекты модели и представления и связывает их воедино.
    DJView view;

    public BeatController(BeatModelInterface model, DJView view) {
        this.model = model;
        view = new DJView(this, model);
        view.createView();                  // Контроллер получает модель в конструкторе
        view.createControls();              // и создает представление
        view.disableStopMenuItem();
        view.enableStartMenuItem();
        model.initialize();
    }

    @Override
    public void start() {             // При выборе команды Start контроллер активизирует модель и изменяет пользовательский интерфейс:
        model.on();                   // команда Start блокируетс, а команда Stop становится доступной.
        view.disableStartMenuItem();
        view.enableStopMenuItem();
    }

    @Override
    public void stop() {              // И наоборот...
        model.off();
        view.disableStopMenuItem();
        view.enableStartMenuItem();
    }

    @Override
    public void increaseBPM() {       // При щелчке на кнопке увеличения контроллер получает текущую частоту от модели,
        int bpm = model.getBPM();     // увеличивает её на 1 и задает результат как новое значение частоты.
        model.setBPM(bpm + 1);
    }

    @Override
    public void decreaseBPM() {       // И наоборот...
        int bpm = model.getBPM();
        model.setBPM(bpm - 1);
    }

    @Override
    public void setBPM(int bpm) {     // Если пользователь ввел произвольную частоту, контроллер приказывает модели перейти на новое значение.
        model.setBPM(bpm);
    }
}*/
