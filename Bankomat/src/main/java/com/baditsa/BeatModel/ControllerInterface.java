package com.baditsa.BeatModel;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 07.08.15
 * Time: 16:09
 * To change this template use File | Settings | File Templates.
 */
public interface ControllerInterface {
    void start();

    void stop();

    void increaseBPM();

    void decreaseBPM();

    void setBPM(int bpm);
}