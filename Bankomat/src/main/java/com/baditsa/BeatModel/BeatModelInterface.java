/*
package com.baditsa.BeatModel;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 05.08.15
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 *//*

public interface BeatModelInterface {

    // Методы, используемые контроллером для управления моделью на основании действий пользователя
    void initialize(); // Вызывается после создания экземпляра BeatModel
    void on(); // Методы запуска
    void off(); // и остановки генератора ритма
    void setBPM(int bpm); // Метод задает частоту ритма (удары в минуту). Частота изменяется сразу же после его вызова.

    // Методы, при помощи которых представление и контроллер получают информацию состояния и изменяют свой статус наблюдателя
    int getBPM(); // Метод возвращает текущую частоту или 0, если генератор отключен.
    void registerObserver(BeatObserver o); // Методы регистрации объектов для оповещения об изменениях состояния.
    void removeObserver(BeatObserver o); // Наблюдатели делятся на 2 группы: те, которые должны оповещаться о каждом ударе,
    void registerObserver(BPMObserver o); // и те, которые должны оповещаться только об изменениях частоты.
    void removeObserver(BPMObserver o);
}*/
