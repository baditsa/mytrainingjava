/*
package com.baditsa.CompositePattern;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 *//*

public class DuckFactory extends AbstractDuckFactory { // DuckFactory расширяет абстрактную фабрику

    // Каждый метод создает продукт: конкретную разновидность утки. Программа не знает фактический класс создаваемого продукта - ей известно лишь то, что создается реализация Quackable
    @Override
    public Quackable createMallardDuck() {
        return new MallardDuck();
    }

    @Override
    public Quackable createRedheadDuck() {
        return new RedheadDuck();
    }

    @Override
    public Quackable createDuckCall() {
        return new DuckCall();
    }

    @Override
    public Quackable createRubberDuck() {
        return new RubberDuck();
    }
}
*/
