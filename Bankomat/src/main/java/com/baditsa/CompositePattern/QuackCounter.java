/*
package com.baditsa.CompositePattern;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 03.08.15
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 *//*

public class QuackCounter implements Quackable { // Декоратор
    Quackable duck; // Переменная для хранения декорируемого объекта
    static int numberOfQuacks; // Для подсчета всех кряков используется статическая переменная

    public QuackCounter(Quackable duck) {
        this.duck = duck; //    В конструкторе получаем ссылку на декорируемую реализацию Quackable
    }

    @Override
    public void quack() {
        duck.quack();     // Вызов quack() делегируется декорируемой реализации Quackable
        numberOfQuacks++; // После чего увеличиваем счетчик
    }

    public static int getQuacks() {
        return numberOfQuacks; // Декоратор дополняется статическим методом, который возвращает количество кряков во всех реализациях Quackable
    }
}*/
