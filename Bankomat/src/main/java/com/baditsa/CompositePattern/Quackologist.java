package com.baditsa.CompositePattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */
public class Quackologist implements Observer { // Наблюдатель должен реализовать интерфейс Observer, иначе его не удастся зарегистрировать с QuackObservable.

    @Override
    // Метод update() выводит информацию о реализации Quackable, от которой поступило оповещение
    public void update(QuackObservable duck) {
        System.out.println("Quackologist: " + duck + " just quacked.");
    }
}
