package com.baditsa.CompositePattern;

import com.baditsa.EnumerationIterator.Iterator;

import java.util.ArrayList;
import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 15:05
 * To change this template use File | Settings | File Templates.
 */
public class Observable implements QuackObservable {
    ArrayList observers = new ArrayList();
    QuackObservable duck;

    public Observable(QuackObservable duck) { // Конструктору передается объект QuackObservable, который используется им для управления наблюдением.
        this.duck = duck;
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers() {
        Iterator iterator = (Iterator) observers.iterator();
        while (iterator.hasNext()) {
            Observer observer = (Observer) iterator.next();
            // observer.update(duck);
        }
    }
}