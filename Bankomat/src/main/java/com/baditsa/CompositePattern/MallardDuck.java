package com.baditsa.CompositePattern;

import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 31.07.15
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
public class MallardDuck implements Quackable {
    Observable observable; // Каждая реализация Quackable содержит объект Observable.

    public MallardDuck() {
        observable = new Observable(this); // В конструкторе создаем объект Observable и передаем ему ссылку на объект MallardDuck
    }

    @Override
    public void quack() {
        System.out.println("Quack");
        notifyObservers(); // Наблюдатели оповещаются о вызовах quack()
    }

    @Override
    public void registerObserver(Observer observer) {
        observable.registerObserver(observer);
    }

    @Override
    public void notifyObservers() {
        observable.notifyObservers();
    }
}