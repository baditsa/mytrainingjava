package com.baditsa.CompositePattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
public interface Observer {
    public void update(QuackObservable duck);
}
