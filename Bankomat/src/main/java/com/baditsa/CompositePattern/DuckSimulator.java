/*
package com.baditsa.CompositePattern;

import java.util.*;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 31.07.15
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 *//*

public class DuckSimulator {
    public static void main(String[] args) {
        DuckSimulator simulator = new DuckSimulator();
        AbstractDuckFactory duckFactory = new CountingDuckFactory(); // Сначала создаем фабрику, которая будет передаваться методу simulate().
        simulator.simulate(duckFactory);
    }

    void simulate(AbstractDuckFactory duckFactory) { // Метод simulate() получает AbstractDuckFactory и использует фабрику для создания уток (вместо непосредственного создания экземпляров).
        Quackable mallardDuck = duckFactory.createMallardDuck();
        Quackable redheadDuck = duckFactory.createRedheadDuck();
        Quackable duckCall = duckFactory.createDuckCall();
        Quackable rubberDuck = duckFactory.createRubberDuck();
        Quackable gooseDuck = new GooseAdapter(new Goose()); // Объекты Goose не декорируются
        System.out.println("\nDuck Simulator: With Composite -  Flocks");

        // Создаем объект Flock и заполняем его реализациями Quackable
        Flock flockOfDucks = new Flock();
        flockOfDucks.add(redheadDuck);
        flockOfDucks.add(duckCall);
        flockOfDucks.add(rubberDuck);
        flockOfDucks.add(gooseDuck);

        // Затем создаем новый объект Flock, предназначенный только для кряков (MallardDuck)
        Flock flockOfMallards = new Flock();
        // Создаем несколько объектов...
        Quackable mallardOne = duckFactory.createMallardDuck();
        Quackable mallardTwo = duckFactory.createMallardDuck();
        Quackable mallardThree = duckFactory.createMallardDuck();
        Quackable mallardFour = duckFactory.createMallardDuck();
        // ...и добавляем их в контейнер Flock
        flockOfMallards.add(mallardOne);
        flockOfMallards.add(mallardTwo);
        flockOfMallards.add(mallardThree);
        flockOfMallards.add(mallardFour);

        // А теперь стая крякв добавляется в основную стаю
        flockOfDucks.add(flockOfMallards);
        System.out.println("\nDuck Simulator: With Observer");
        Quackologist quackologist = new Quackologist();
        flockOfDucks.registerObserver((java.util.Observer) quackologist);


        simulate(flockOfDucks); // Сначала тестируем всю стаю

        System.out.println("\nDuck Simulator: Mallard Flock Simulation");
        simulate(flockOfMallards); // А теперь - только стаю крякв

        System.out.println("The ducks quacked " + QuackCounter.getQuacks() + " times");
    }

    void simulate(Quackable duck) {
        duck.quack();
    }
}*/
