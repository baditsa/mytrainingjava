/*
package com.baditsa.CompositePattern;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 *//*

public class CountingDuckFactory extends AbstractDuckFactory {

    // Каждый метод упаковывает Quackable в декоратор. Программа этого не замечает: она получает то, что ей нужно, т.е. реализацию Quackable.
    // Каждый кряк будет подсчитан

    @Override
    public Quackable createMallardDuck() {
        return new QuackCounter(new MallardDuck());
    }

    @Override
    public Quackable createRedheadDuck() {
        return new QuackCounter(new RedheadDuck());
    }

    @Override
    public Quackable createDuckCall() {
        return new QuackCounter(new DuckCall());
    }

    @Override
    public Quackable createRubberDuck() {
        return new QuackCounter(new RubberDuck());
    }
}*/
