/*
package com.baditsa.CompositePattern;

import com.baditsa.EnumerationIterator.Iterator;

import java.util.ArrayList;

*/
/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 *//*

public class Flock implements Quackable {
    ArrayList quackers = new ArrayList(); // Каждая стая (Flock) содержит ArrayList для хранения реализаций Quackable, входящих в эту стаю

    public void add(Quackable quacker) { // Метод add() включает реализацию Quackable в Flock
        quackers.add(quacker);
    }

    @Override
    public void quack() {
        Iterator iterator = (Iterator) quackers.iterator();
        while (iterator.hasNext()) {
            Quackable quacker = (Quackable)iterator.next();
            quacker.quack(); // Метод quack() для объекта Flock должен применяться ко всем уткам стаи. Мы перебираем элементы ArrayList и вызываем quack() для каждого элемента
        }
    }
}
*/
