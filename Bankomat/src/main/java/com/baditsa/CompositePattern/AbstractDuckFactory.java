package com.baditsa.CompositePattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 12:38
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDuckFactory { // Определяем абстрактную фабрику, которая будет реализовываться субклассами для создания разных продуктоа

    public abstract Quackable createMallardDuck();

    public abstract Quackable createRedheadDuck();

    public abstract Quackable createDuckCall();

    public abstract Quackable createRubberDuck();
}
