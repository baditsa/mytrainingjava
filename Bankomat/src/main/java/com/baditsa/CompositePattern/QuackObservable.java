package com.baditsa.CompositePattern;

import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 04.08.15
 * Time: 14:54
 * To change this template use File | Settings | File Templates.
 */
public interface QuackObservable { // Чтобы за Quackable можно было наблюдать, они должны реализовать интерфейс QuackObservable.

    public void registerObserver(Observer observer); // Метод регистрации наблюдателей. Любой объект, реализующий интерфейс Observer, сможет получать оповещения

    public void notifyObservers(); // Метод оповещения наблюдателей
}
