package com.baditsa.CompositePattern;

/**
 * Created with IntelliJ IDEA.
 * User: Alex
 * Date: 31.07.15
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
public interface Quackable extends QuackObservable {
    public void quack();
}
